#ifndef OPTIONSFILE_ERRORS_H
#define OPTIONSFILE_ERRORS_H

#define OF_STRMAXLEN 256

enum OFErrorType { ofeUnknown, ofeTooLongString, ofeWrongFormat };

class OFError {
public:
	OFError(OFErrorType e = ofeUnknown, int x = 0):
	 es(""), et(e), num(x) {}
	OFError(const char *e):
	 es(e), et(ofeUnknown), num(0) {}

	void Print() const;

private:
	const char *es;
	OFErrorType et;
	int num;
};

#endif // OPTIONSFILE_ERRORS_H
