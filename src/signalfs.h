#ifndef SIGNALFS_H
#define SIGNALFS_H

#include "cchain.h"
#include "menuobject.h"

void Action_SwitchPage(MenuObject *, cChain<void *> *);
void Action_GrabKey(MenuObject *, cChain<void *> *);

void Action_ResumeGame(MenuObject *, cChain<void *> *);
void Action_NewGame(MenuObject *, cChain<void *> *);
void Action_Exit(MenuObject *, cChain<void *> *);

void Action_ToggleFS(MenuObject *, cChain<void *> *);
void Action_BrowseStyle(MenuObject *, cChain<void *> *);
void Action_BrowseKickCursor(MenuObject *, cChain<void *> *);

#endif // SIGNALFS_H
