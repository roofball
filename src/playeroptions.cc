#include "playeroptions.h"
#include "error.h"
#include <iostream>
#include <cstring>

PlayerOptions::PlayerOptions():
 update(false) {
}

PlayerOptions::~PlayerOptions() {
}

void PlayerOptions::SetStyle(const char *n) {
	strcpy(name, n);

	if (strcmp(name, "Lake") == 0) {
		colors.hair  = 0x888888;
		colors.shirt = 0;//x4a4a4a;
		colors.pants = 0xc1b487;
		colors.shoes = 0x444444;
	}
	else if (strcmp(name, "Make") == 0) {
		colors.hair  = 0xff7700;//ef9802;
		colors.shirt = 0x1f00a0;
		colors.pants = 0xa04c30;
		colors.shoes = 0xffffff;
	}
	else if (strcmp(name, "Heke") == 0) {
		colors.hair  = 0xa05c09;
		colors.shirt = 0xbb1111;
		colors.pants = 0x505065;
		colors.shoes = 0xe5b357;
	}
	else if (strcmp(name, "Tume") == 0) {
		colors.hair  = 0xaa9336;
		colors.shirt = 0x5f7773;
		colors.pants = 0xa09a82;
		colors.shoes = 0x0000bb;
	} else throw Error("PlayerOptions::SetStyle: Invalid player style requested");

	update = true;
}

const char *PlayerOptions::GetName() const {
	return (name);
}

const CharacterColors &PlayerOptions::GetColors() const {
	return (colors);
}
