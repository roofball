#include "menu.h"
#include "error.h"
#include "defines.h"
#include "enums.h"
#include <iostream>

Menu::Menu(SDL_Surface *s, const char *p, Options *opt):
 data(new MenuData(p)),
 screen(s),
 options(opt),
 current(&main) {
	if (!data) throw Error(etNoMem);
	SDL_SetAlpha(data->bg, SDL_SRCALPHA, 0xe6);
}

Menu::~Menu() {
	delete (data);
}

void Menu::Run(ResumeMode &mode) {
	// Refresh entire screen to menu graphics
	SDL_BlitSurface(data->bg, NULL, screen, NULL);

	// Copy background image for further usage
	this->bg = SDL_DisplayFormat(screen);
	if (!this->bg) throw Error(etNoMem);

	// Draw current menu
	current->Draw(screen);
	SDL_Flip(screen);

	SDL_EnableKeyRepeat(300, 112);

	SDL_Event events;
	update = false;

	while (mode == rmMenu) {
		HandleEvents(events, mode);

		if (update) {
			// Undraw previous page
			SDL_BlitSurface(bg, NULL, screen, NULL);

			current->Draw(screen);
			SDL_Flip(screen);

			update = false;
		}
	}

	SDL_FreeSurface(bg);
}

void Menu::SwitchPage(MenuPage *new_page) {
	current = new_page;
	update = true;

	// Shade background region
	if (new_page->clipRect) {
		// Create a slightly opaque black surface
		SDL_Surface *surf = SDL_CreateRGBSurface(SDL_SRCALPHA|SDL_SRCCOLORKEY,
					SCR_RESO_X, SCR_RESO_Y, SCR_BPP,
					0x00ff0000, 0x0000ff00, 0x000000ff, 0);
		if (!surf) throw Error(etNoMem);
		SDL_SetAlpha(surf, SDL_SRCALPHA, 0x20);
		SDL_FillRect(surf, NULL, 0x0000);

		// Don't shade the cliprect area
		SDL_SetColorKey(surf, SDL_SRCCOLORKEY, 0xff00);
		SDL_Rect rect(*new_page->clipRect);
		SDL_FillRect(surf, &rect, 0xff00);

		SDL_BlitSurface(surf, NULL, screen, NULL);
		SDL_FreeSurface(surf);
	}

	SDL_SetClipRect(screen, current->clipRect);
}

void Menu::Undraw(SDL_Rect *r) {
	SDL_BlitSurface(bg, r, screen, r);
}

void Menu::HandleEvents(SDL_Event &event, ResumeMode &mode) {
	while (SDL_PollEvent(&event))
	switch (event.type) {
	case SDL_KEYDOWN:
#ifdef MENUDEVEL
		SDL_ShowCursor(SDL_DISABLE);
#endif

		switch (event.key.keysym.sym) {
		case SDLK_u:
		case SDLK_ESCAPE:
			if (current->parent)
				SwitchPage(current->parent);
			else
				mode = rmGame;
			break;

		case SDLK_q:
			mode = rmQuit;
			break;

		case SDLK_m:
		case SDLK_BACKSPACE:
			SwitchPage(&this->main);
			break;

		case SDLK_p:
		case SDLK_UP:
			current->PollSelected(pdPrev);
			update = true;
			break;

		case SDLK_n:
		case SDLK_DOWN:
			current->PollSelected(pdNext);
			update = true;
			break;

		case SDLK_r:
		case SDLK_RETURN:
			current->RunSelected();
			break;

		default:
			break;
		}
		break;

#ifdef MENUDEVEL
	case SDL_MOUSEBUTTONDOWN:
		cout << "X: " << event.button.x << " Y:" << event.button.y << endl;
		mode = rmQuit;
		break;

	case SDL_MOUSEMOTION:
		SDL_ShowCursor(SDL_ENABLE);
		break;
#endif

	case SDL_QUIT:
		mode = rmQuit;
		break;

	default:
		break;
	}
}
