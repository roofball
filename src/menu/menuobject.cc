#include "menuobject.h"
#include "defines.h"
#include "error.h"

MenuObject::MenuObject():
 enabled(true),
 visible(true),
 afunct(NULL),
 parms(NULL) {
}

MenuObject::~MenuObject() {
	if (parms) delete parms;
}

void MenuObject::SetPos(int x, int y) {
	throw Error("MenuObject SetPos method not properly set.");
}

void MenuObject::SetColor(Color c) {
	throw Error("MenuObject SetColor method not properly set.");
}

void MenuObject::Draw(SDL_Surface *t) {
	throw Error("MenuObject Draw method not properly set.");
}

const int MenuObject::Width() const {
	throw Error("MenuObject Width method not properly set.");
}

const int MenuObject::Height() const {
	throw Error("MenuObject Height method not properly set.");
}

void MenuObject::SetFunct(void (*f)(MenuObject*, cChain<void*>*)) {
	afunct = f;
}

void MenuObject::AddParm(void *p) {
	if (!parms) {
		parms = new cChain<void*>;
		if (!parms) throw Error(etNoMem);
	}

	parms->PushBack(p);
}

void MenuObject::RunFunct() {
	if (afunct)
		afunct(this, parms);
	else
		std::cout << "No function\n";
}

bool MenuObject::Enabled() const {
	return (enabled);
}

void MenuObject::SetEnabled(bool e) {
	SetColor(e ? MO_COLOR_ENABLED : MO_COLOR_DISABLED);
	enabled = e;
}

bool MenuObject::Visible() const {
	return (visible);
}

void MenuObject::SetVisible(bool v) {
	visible = v;
}
