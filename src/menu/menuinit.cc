#include "menu.h"
#include "defines.h"
#include "enums.h"

#include "signalfs.h"
#include "getkeyname.h"

#include "moframe.h"
#include "motext.h"
#include "mocheckbox.h"
#include "modroplist.h"
#include "modisplay.h"

MenuPage *NewPage();
MenuPage *NewPage(SDL_Rect &);
MenuObject *NewFrame();
MenuObject *NewFrame(SDL_Rect &);
MenuObject *NewText(Font *, const char *, bool = true);
MenuObject *NewLabel(Font *, const char *);
MenuObject *NewCheckBox(const MenuData *, bool, Font *, const char *);
MenuObject *NewDropList(const MenuData *, Font *, Uint16 = 2);

void Menu::Init(Game &game, ResumeMode &mode) {
	MenuPage *new_page;
	MenuObject *new_obj, *tmp_obj;

	// Resume Game
	new_obj = NewText(data->lFont, "Resume Game");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width()) / 2, 18);
	new_obj->SetEnabled(false);
	new_obj->SetFunct(Action_ResumeGame);
	new_obj->AddParm(&mode);
	main.AddObject(new_obj);

	tmp_obj = new_obj;

	// New Game
	new_obj = NewText(data->lFont, "New Game");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width()) / 2, 58);
	new_obj->SetFunct(Action_NewGame);
	new_obj->AddParm(&game);
	new_obj->AddParm(&mode);
	new_obj->AddParm(tmp_obj);
	main.AddObject(new_obj);

	// Options
	new_page = InitOptions();
	main.AddPage(new_page);
	new_obj = NewText(data->lFont, "Options");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width()) / 2, 98);
	new_obj->SetFunct(Action_SwitchPage);
	new_obj->AddParm(this);
	new_obj->AddParm(new_page);
	main.AddObject(new_obj);

	// Info
	new_page = InitInfo();
	new_obj = NewText(data->lFont, "Info");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width()) / 2, 138);
	new_obj->SetFunct(Action_SwitchPage);
	new_obj->AddParm(this);
	new_obj->AddParm(new_page);
	main.AddObject(new_obj);
	main.AddPage(new_page);

	// Exit
	new_obj = NewText(data->lFont, "Exit");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width()) / 2, 178);
	new_obj->SetFunct(Action_Exit);
	new_obj->AddParm(&mode);
	main.AddObject(new_obj);
}

MenuPage *Menu::InitOptions() {
	MenuObject *new_obj;
	MenuPage *new_page, *pl1_page, *pl2_page;

	new_page = NewPage();

	new_obj = NewCheckBox(data, true, data->sFont, "FullScreen mode");
	new_obj->SetPos(99, 8);
	if (options->fullScreen)
		((moCheckBox *)new_obj)->SetChecked(true);
	new_obj->SetFunct(Action_ToggleFS);
	new_obj->AddParm(this);
	new_obj->AddParm(options);
	new_obj->AddParm(screen);
	new_obj->AddParm(&update);
	new_page->AddObject(new_obj);

	SDL_Rect PLOptArea = { 99, 34, 186, 175 };
	new_obj = NewFrame(PLOptArea);
	new_page->AddObject(new_obj);

	pl1_page = InitPlayerOptions(&options->pl1, new_page, PLOptArea);
	new_page->AddPage(pl1_page);
	new_obj = NewText(data->mFont, "Player 1");
	new_obj->SetPos(28, 54);
	new_obj->SetFunct(Action_SwitchPage);
	new_obj->AddParm(this);
	new_obj->AddParm(pl1_page);
	new_page->AddObject(new_obj);

	pl2_page = InitPlayerOptions(&options->pl2, new_page, PLOptArea);
	new_page->AddPage(pl2_page);
	new_obj = NewText(data->mFont, "Player 2");
	new_obj->SetPos(27, 84);
	new_obj->SetFunct(Action_SwitchPage);
	new_obj->AddParm(this);
	new_obj->AddParm(pl2_page);
	new_page->AddObject(new_obj);

	new_obj = NewText(data->mFont, "Return to main");
	new_obj->SetPos(104, 212);
	new_obj->SetFunct(Action_SwitchPage);
	new_obj->AddParm(this);
	new_obj->AddParm(&main);
	new_page->AddObject(new_obj);

	return (new_page);
}

MenuPage *Menu::InitPlayerOptions(PlayerOptions *pOpt, MenuPage *oPage, SDL_Rect &area) {
	MenuObject *new_obj;
	MenuPage *new_page;

	new_page = NewPage(area);

	new_obj = NewText(data->sFont, "Style:", false);
	new_obj->SetPos(153, 41);
	new_page->AddObject(new_obj);

	new_obj = NewDropList(data, data->sFont);
	new_obj->SetPos(192, 39);
	((moDropList *)new_obj)->AddItem("Lake", iaCenter);
	((moDropList *)new_obj)->AddItem("Make", iaCenter);
	((moDropList *)new_obj)->AddItem("Heke", iaCenter);
	((moDropList *)new_obj)->AddItem("Tume", iaCenter);
	((moDropList *)new_obj)->SetCurrent(pOpt->GetName());
	new_obj->SetFunct(Action_BrowseStyle);
	new_obj->AddParm(this);
	new_obj->AddParm(pOpt);
	new_obj->AddParm(screen);
	new_obj->AddParm(&update);
	new_page->AddObject(new_obj);

	new_obj = NewFrame(area);
	((moFrame *)new_obj)->SetColor(MO_COLOR_SELECTED);
	new_page->AddObject(new_obj);

	new_obj = NewText(data->sFont, "Move Left:", false);
	new_obj->SetPos(205 - new_obj->Width(), 72);
	new_page->AddObject(new_obj);
	new_obj = NewText(data->sFont, GetKeyName(pOpt->keys.left));
	new_obj->SetPos(210, 72);
	new_obj->SetFunct(Action_GrabKey);
	new_obj->AddParm(this);
	new_obj->AddParm(&pOpt->keys.left);
	new_obj->AddParm(screen);
	new_page->AddObject(new_obj);

	new_obj = NewText(data->sFont, "Move Right:", false);
	new_obj->SetPos(205 - new_obj->Width(), 88);
	new_page->AddObject(new_obj);
	new_obj = NewText(data->sFont, GetKeyName(pOpt->keys.right));
	new_obj->SetPos(210, 88);
	new_obj->SetFunct(Action_GrabKey);
	new_obj->AddParm(this);
	new_obj->AddParm(&pOpt->keys.right);
	new_obj->AddParm(screen);
	new_page->AddObject(new_obj);

	new_obj = NewText(data->sFont, "Kick Left:", false);
	new_obj->SetPos(205 - new_obj->Width(), 104);
	new_page->AddObject(new_obj);
	new_obj = NewText(data->sFont, GetKeyName(pOpt->keys.kickLeft));
	new_obj->SetPos(210, 104);
	new_obj->SetFunct(Action_GrabKey);
	new_obj->AddParm(this);
	new_obj->AddParm(&pOpt->keys.kickLeft);
	new_obj->AddParm(screen);
	new_page->AddObject(new_obj);

	new_obj = NewText(data->sFont, "Kick Up:", false);
	new_obj->SetPos(205 - new_obj->Width(), 120);
	new_page->AddObject(new_obj);
	new_obj = NewText(data->sFont, GetKeyName(pOpt->keys.kickUp));
	new_obj->SetPos(210, 120);
	new_obj->SetFunct(Action_GrabKey);
	new_obj->AddParm(this);
	new_obj->AddParm(&pOpt->keys.kickUp);
	new_obj->AddParm(screen);
	new_page->AddObject(new_obj);

	new_obj = NewText(data->sFont, "Kick Right:", false);
	new_obj->SetPos(205 - new_obj->Width(), 136);
	new_page->AddObject(new_obj);
	new_obj = NewText(data->sFont, GetKeyName(pOpt->keys.kickRight));
	new_obj->SetPos(210, 136);
	new_obj->SetFunct(Action_GrabKey);
	new_obj->AddParm(this);
	new_obj->AddParm(&pOpt->keys.kickRight);
	new_obj->AddParm(screen);
	new_page->AddObject(new_obj);


	new_obj = NewText(data->sFont, "Kick cursor:", false);
	new_obj->SetPos(127, 168);
	new_page->AddObject(new_obj);

	new_obj = NewDropList(data, data->sFont);
	new_obj->SetPos(204, 165);
	((moDropList *)new_obj)->AddItem("Always", iaCenter);
	((moDropList *)new_obj)->AddItem("OnKick", iaCenter);
	((moDropList *)new_obj)->AddItem("Never", iaCenter);
	switch (pOpt->aim) {
	case amAlways: ((moDropList *)new_obj)->SetCurrent("Always");	break;
	case amOnKick: ((moDropList *)new_obj)->SetCurrent("OnKick");	break;
	case amNever:  ((moDropList *)new_obj)->SetCurrent("Never");	break;	
	}
	new_obj->SetFunct(Action_BrowseKickCursor);
	new_obj->AddParm(this);
	new_obj->AddParm(pOpt);
	new_obj->AddParm(screen);
	new_obj->AddParm(&update);
	new_page->AddObject(new_obj);

	new_obj = NewText(data->sFont, "Done");
	new_obj->SetPos(177, 189);
	new_obj->SetFunct(Action_SwitchPage);
	new_obj->AddParm(this);
	new_obj->AddParm(oPage);
	new_page->AddObject(new_obj);

	return (new_page);
}

MenuPage *Menu::InitInfo() {
	MenuObject *new_obj;
	MenuPage *new_page;

	new_page = NewPage();

	new_obj = NewLabel(data->sFont, "RoofBall v" VERSION);
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 0);
	new_page->AddObject(new_obj);

	new_obj = NewLabel(data->sFont, "Authors:");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 27);
	new_page->AddObject(new_obj);

	new_obj = NewLabel(data->sFont, "Henrik Ala-Uotila");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 45);
	new_page->AddObject(new_obj);

	new_obj = NewLabel(data->sFont, "Tuomas Suutari");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 63);
	new_page->AddObject(new_obj);

	new_obj = NewFrame();
	((moFrame *)new_obj)->SetSize(184, 111);
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 91);
	new_page->AddObject(new_obj);

	new_obj = NewLabel(data->sFont, "RoofBall is still in an early");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 97);
	new_page->AddObject(new_obj);

	new_obj = NewLabel(data->sFont, "development phase. Newer");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 117);
	new_page->AddObject(new_obj);

	new_obj = NewLabel(data->sFont, "versions will be available");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 137);
	new_page->AddObject(new_obj);

	new_obj = NewLabel(data->sFont, "at The Mystic Tunes' Web");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 157);
	new_page->AddObject(new_obj);

	new_obj = NewLabel(data->sFont, "Site( http://tmt.listen.to/ ).");
	new_obj->SetPos((SCR_RESO_X - new_obj->Width())/2, 177);
	new_page->AddObject(new_obj);

 	new_obj = NewText(data->mFont, "Return to main");
	new_obj->SetPos((SCR_RESO_X-new_obj->Width())/2, SCR_RESO_Y-new_obj->Height()-6);
	new_obj->SetFunct(Action_SwitchPage);
	new_obj->AddParm(this);
	new_obj->AddParm(&main);
	new_page->AddObject(new_obj);

	return (new_page);
}

MenuPage *NewPage() {
	MenuPage *temp(new MenuPage);
	if (!temp) throw Error(etNoMem);
	return (temp);
}

MenuPage *NewPage(SDL_Rect &cr) {
	MenuPage *temp(new MenuPage(&cr));
	if (!temp) throw Error(etNoMem);
	return (temp);
}

MenuObject *NewFrame() {
	MenuObject *temp(new moFrame);
	if (!temp) throw Error(etNoMem);
	return (temp);
}

MenuObject *NewFrame(SDL_Rect &rect) {
	MenuObject *temp(new moFrame(rect));
	if (!temp) throw Error(etNoMem);
	return (temp);
}

MenuObject *NewText(Font *f, const char *c, bool b) {
	MenuObject *temp(new moText(f, c, b));
	if (!temp) throw Error(etNoMem);
	return (temp);
}

MenuObject *NewLabel(Font *f, const char *c) {
	MenuObject *temp(new moText(f, c, false));
	if (!temp) throw Error(etNoMem);
	return (temp);
}

MenuObject *NewCheckBox(const MenuData *d, bool b, Font *f, const char *t) {
	MenuObject *temp(new moCheckBox(d, b, f, t));
	if (!temp) throw Error(etNoMem);
	return (temp);
}

MenuObject *NewDropList(const MenuData *d, Font *f, Uint16 b) {
	MenuObject *temp;
	temp = new moDropList(d, f, b);
	if (!temp) throw Error(etNoMem);
	return (temp);
}
