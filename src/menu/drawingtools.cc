#include "drawingtools.h"

void DrawRect(SDL_Surface *t, int x, int y, int w, int h, Color color) {
	Uint32 *pxl;
	Sint32 i;

	pxl = (Uint32 *)t->pixels + y * t->pitch / t->format->BytesPerPixel + x;

	for (i = 1; i < w; ++i) {
		pxl += 1;
		*pxl = color;
	}

	for (i = 1; i < h; ++i) {
		pxl += t->w;
		*pxl = color;
	}

	for (i = 1; i < w; ++i) {
		pxl -= 1;
		*pxl = color;
	}

	for (i = 1; i < h; ++i) {
		pxl -= t->w;
		*pxl = color;
	}
}

void DrawRect(SDL_Surface *t, SDL_Rect r, Color c) {
	DrawRect(t, r.x, r.y, r.w, r.h, c);
}

void DrawLine(SDL_Surface *s, int sx, int sy, int ex, int ey, Color color) {
	const Sint32
		maxx = (sx > ex) ? sx : ex,
		maxy = (sy > ey) ? sy : ey,
		minx = (sx < ex) ? sx : ex,
		miny = (sy < ey) ? sy : ey,
		dx2 = (maxx - minx) << 1, // 2 * dx
		dy2 = (maxy - miny) << 1; // 2 * dy
	Sint32 step, d, px, py;
	Uint32 *pxl;

	pxl = (Uint32*)s->pixels;

	if (dx2 > dy2) {
		py = (sx < ex) ? sy : ey;
		step = (py < maxy) ? 1 : -1;
		d = maxx - minx; // dx

		for (px = minx; px <= maxx; ++px) {
			DrawPixel(s, px, py, color);
			d -= dy2;
			if (d < 0) {
				d += dx2;
				py += step;
			}
		}
	}
	else {
		px = (sy < ey) ? sx : ex;
		step = (px < maxx) ? 1 : -1;
		d = maxy - miny; // dy

		for (py = miny; py <= maxy; ++py) {
			DrawPixel(s, px, py, color);
			d -= dx2;
			if (d < 0) {
				d += dy2;
				px += step;
			}
		}
	}
}

/* PREV VERSION - worked 
void DrawLine(SDL_Surface *s, int sx, int sy, int ex, int ey, SDL_Color c) {
	const Sint32
		maxx = (sx > ex) ? sx : ex,
		maxy = (sy > ey) ? sy : ey,
		minx = (sx < ex) ? sx : ex,
		miny = (sy < ey) ? sy : ey,
		dx2 = (maxx - minx) << 1, // 2 * dx
		dy2 = (maxy - miny) << 1; // 2 * dy
	Sint32 step, d, px, py;
	Uint32 *pxl, color;

	pxl = (Uint32*)s->pixels;
	color = SDL_MapRGB(s->format, c.r, c.g, c.b);

	if (dx2 > dy2) {
		py = (sx < ex) ? sy : ey;
		step = (py < maxy) ? 1 : -1;
		d = maxx - minx; // dx

		for (px = minx; px <= maxx; ++px) {
			DrawPixel(s, px, py, c);
			d -= dy2;
			if (d < 0) {
				d += dx2;
				py += step;
			}
		}

		pxl += s->w + 1;
		*pxl = 0xffff0000;
	}
	else {
		px = (sy < ey) ? sx : ex;
		step = (px < maxx) ? 1 : -1;
		d = maxy - miny; // dy

		for (py = miny; py <= maxy; ++py) {
			DrawPixel(s, px, py, c);
			d -= dx2;
			if (d < 0) {
				d += dy2;
				px += step;
			}
		}
	}
}
*/

void DrawPixel(SDL_Surface *s, int x, int y, Color c) {
	Uint32 *pBuffer = (Uint32*)s->pixels;
	pBuffer[y * s->w + x] = c;
}

/*	Circle drawing routine 		*

void PlotCircle(SDL_Surface *, Sint16, Sint16, Sint16, Sint16, Uint32);

void DrawCircle(SDL_Surface *s, Sint16 mx, Sint16 my, Uint16 r, SDL_Color c) {
	Sint16
		dx = 0,
		dy = r;
	Sint32	p = 3 - 2 * r;
	Uint32 color = SDL_MapRGB(s->format, c.r, c.g, c.b);

	while (dx < dy) {
		PlotCircle(s, mx, my, dx, dy, color);
		if (p < 0)
			p += 4 * dx + 6;
		else {
			p += 4 * (dx - dy) + 10;
			--dy;
		}
		++dx;
	}
	if (dx == dy)
		PlotCircle (s, mx, my, dx, dy, color);
}

void PlotCircle(SDL_Surface *s, Sint16 mx, Sint16 my, Sint16 dx, Sint16 dy, Uint32 color) {

	// mx = middle x, dx = (horisontal) distance from middle, etc.

	DrawPixel(s, mx + dx, my + dy, color);
	DrawPixel(s, mx - dx, my + dy, color);
	DrawPixel(s, mx + dx, my - dy, color);
	DrawPixel(s, mx - dx, my - dy, color);
	DrawPixel(s, mx + dy, my + dx, color);
	DrawPixel(s, mx - dy, my + dx, color);
	DrawPixel(s, mx + dy, my - dx, color);
	DrawPixel(s, mx - dy, my - dx, color);
}
*/
