#include "menupage.h"
#include "defines.h"
#include "error.h"

MenuPage::MenuPage(SDL_Rect *cr):
 parent(NULL),
 clipRect(cr ? new SDL_Rect(*cr) : NULL) {
	if (cr && !clipRect) throw Error(etNoMem);
}

MenuPage::~MenuPage() {
	Uint32 i;

	for (i = 0; i < objects.Size(); ++i)
		delete (objects.Next());

	for (i = 0; i < pages.Size(); ++i)
		delete (pages.Next());

	if (clipRect) delete clipRect;
}

void MenuPage::AddObject(MenuObject *o) {
	objects.PushBack(o);
}

void MenuPage::AddPage(MenuPage *p) {
	p->parent = this;
	pages.PushBack(p);
}

void MenuPage::PollSelected(PollDir d) {
	MenuObject *old_s(NULL), *new_s(NULL);

	if (objects.IsPolling())
		old_s = objects.Current();

	for (Uint32 i = 0; i < objects.Size(); ++i) {
		if (d == pdNext)
			objects.Next();
		else
			objects.Prev();

		if (objects.Current()->Enabled()) {
			new_s = objects.Current();
			break;
		}
	}

	if (old_s) old_s->SetColor(old_s->Enabled() ? MO_COLOR_ENABLED : MO_COLOR_DISABLED);
	if (new_s) new_s->SetColor(MO_COLOR_SELECTED);
}

void MenuPage::RunSelected() {
	objects.Current()->RunFunct();
}

void MenuPage::Draw(SDL_Surface *s) {
	/* If no object is currently selected, set *
	 * selection to the first available object */
	if (!objects.IsPolling())
		PollSelected(pdNext);


	/* Poll through the entire chain and leave the *
	 * current poll position eventually untouched  */
	for (ulong i = 0; i < objects.Size(); ++i)
		objects.Next()->Draw(s);
}
