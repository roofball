#ifndef MOTEXT_H
#define MOTEXT_H

#include "menuobject.h"
#include "SDL_tbmf_textsurf.h"

using namespace sdl_tbmf;

class moText: public MenuObject {
public:
	moText(Font *, const char *, bool = true);
	~moText();

	void SetPos(int, int);
	void SetColor(Color);
	void Draw(SDL_Surface *);
	const int Width() const;
	const int Height() const;

	void SetText(const char *);
	const char *GetText();
	SDL_Rect GetRect() const;

protected:
	TextSurface ts;
	SDL_Rect trect;
};

#endif // MOTEXT_H
