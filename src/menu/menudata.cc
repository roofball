#include "menudata.h"
#include "defines.h"
#include "error.h"
#include "getgraphic.h"
#include <cstring>

using namespace sdl_tbmf;

MenuData::MenuData(const char *prefix) {
	Uint32
		prelen = strlen(prefix),
		imglen = strlen(IMGDIR),
		fntlen = strlen(FNTDIR);

	char path[PATHSIZE];

	strcpy(path, prefix);

	strcpy(path + prelen, IMGDIR);

	strcpy(path + prelen + imglen, "menubg.bmp");
	GetGraphic(&bg, path);

	strcpy(path + prelen + imglen, "cb_box.bmp");
	GetGraphic(&cb_box, path);

	strcpy(path + prelen + imglen, "cb_check.bmp");
	GetGraphic(&cb_check, path);

	strcpy(path + prelen, FNTDIR);

	strcpy(path + prelen + fntlen, "font_l.tbmf");
	lFont = new Font(path);
	if (!lFont) throw Error("Unable to load font(large)");

	strcpy(path + prelen + fntlen, "font_m.tbmf");
	mFont = new Font(path);
	if (!mFont) throw Error("Unable to load font(medium)");

	strcpy(path + prelen + fntlen, "font_s.tbmf");
	sFont = new Font(path);
	if (!sFont) throw Error("Unable to load font(small)");

	cSelected.r = 0xff;
	cSelected.g = 0x7f;
	cSelected.b = 0x00;

	cEnabled.r = 0x8c;
	cEnabled.g = 0x41;
	cEnabled.b = 0x00;

	cDisabled.r = 0x5a;
	cDisabled.g = 0x5a;
	cDisabled.b = 0x5a;
}

MenuData::~MenuData() {
	SDL_FreeSurface(bg);
	SDL_FreeSurface(cb_check);
	SDL_FreeSurface(cb_box);
}
