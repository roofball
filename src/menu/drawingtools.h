#ifndef DRAWINGTOOLS_H
#define DRAWINGTOOLS_H

#include "color.h"
#include "SDL.h"

void DrawRect(SDL_Surface *, SDL_Rect, Color);
void DrawRect(SDL_Surface *, int, int, int, int, Color);
void DrawLine(SDL_Surface *, int, int, int, int, Color);
void DrawPixel(SDL_Surface *, int, int, Color);

#endif // DRAWINGTOOLS_H
