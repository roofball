#ifndef MENUPAGE_H
#define MENUPAGE_H

#include "menuobject.h"
#include "cchain.h"

enum PollDir { pdNext, pdPrev };

class MenuPage {
public:
	MenuPage(SDL_Rect * = NULL);
	~MenuPage();

	void AddObject(MenuObject *);
	void AddPage(MenuPage *);

	void PollSelected(PollDir);
	void RunSelected();

	void Draw(SDL_Surface *);

	MenuPage *parent;
	const SDL_Rect *clipRect;

private:
	cChain<MenuObject *> objects;
	cChain<MenuPage *> pages;
};

#endif // MENUPAGE_H
