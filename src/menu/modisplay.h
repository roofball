#ifndef MODISPLAY_H
#define MODISPLAY_H

#include "menuobject.h"

class moDisplay: public MenuObject {
public:
	moDisplay();
	~moDisplay();

	void SetPos(int, int);
	void SetColor(Color);
	void Draw(SDL_Surface *);
	const int Width() const;
	const int Height() const;

private:
	SDL_Rect rect;
	SDL_Surface *graph;
};

#endif // MODISPLAY_H
