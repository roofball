#include "mocheckbox.h"
#include "drawingtools.h"
#include "defines.h"
#include "error.h"

moCheckBox::moCheckBox(MenuData const *d, bool c, Font *f, const char *t):
 checked(false),
 text(f, t) {
	graph_check = d->cb_check;
	graph_box = SDL_DisplayFormat(d->cb_box);
	if (!graph_box)
		throw Error("Out of memory or screen not initialized");

	SetColor(MO_COLOR_ENABLED);
}

moCheckBox::~moCheckBox() {
	SDL_FreeSurface(graph_box);
}

void moCheckBox::SetPos(int x, int y) {
	cb_rect.x = x;
	cb_rect.y = y;

	text.SetPos(x + graph_box->w + 4, y - 1);
}

void moCheckBox::SetColor(Color c) {
	SDL_LockSurface(graph_box);
	DrawRect(graph_box, 1, 1, 15, 15, c);
	SDL_UnlockSurface(graph_box);

	text.SetColor(c);
}

void moCheckBox::Draw(SDL_Surface *s) {
	SDL_BlitSurface(graph_box, NULL, s, &cb_rect);
	if (checked) SDL_BlitSurface(graph_check, NULL, s, &cb_rect);

	text.Draw(s);
}

const int moCheckBox::Width() const {
	return (graph_box->w + 4 + text.Width());
}

const int moCheckBox::Height() const {
	if (text.Height() > graph_box->h)
		return (text.Height());
	else
		return (graph_box->h);
}

bool moCheckBox::Checked() const {
	return (checked);
}

void moCheckBox::SetChecked(bool b) {
	checked = b;
}
