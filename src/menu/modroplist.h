#ifndef MODROPLIST_H
#define MODROPLIST_H

#include "menudata.h"
#include "menuobject.h"
#include "SDL_tbmf_textsurf.h"

enum ItemAlign { iaLeft, iaCenter, iaRight };

using namespace sdl_tbmf;

class moDropList: public MenuObject {
public:
	moDropList(const MenuData*, Font*, Uint16 = 3);
	~moDropList();

	void SetPos(int, int);
	void SetColor(Color);
	void Draw(SDL_Surface*);
	const int Width() const;
	const int Height() const;

	void AddItem(const char *, ItemAlign = iaLeft);
	void SetCurrent(const char *);

	const char *Browse(SDL_Surface *);

private:
	void HandleEvents(SDL_Event &);
	void UpdateGraph(SDL_Surface *);
	void DrawBorders(SDL_Surface *);
	void SelNext();
	void SelPrev();

	struct ListItem {
		void AutoPos(SDL_Rect);

		TextSurface *ts;
		Uint32 index;
		ItemAlign align;
		SDL_Rect rect;
	};

	cChain<ListItem *> list;
	const Uint16 borderSize;
	Font *font;

	Color color;
	SDL_Rect rect, irect;
	ListItem *selBef;
	bool open;
};

#endif // MODROPLIST_H
