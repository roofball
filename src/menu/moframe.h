#ifndef _MOFRAME_H
#define _MOFRAME_H

#include "menuobject.h"

class moFrame: public MenuObject {
public:
	moFrame();
	moFrame(SDL_Rect &);
	moFrame(Sint16, Sint16, Uint16, Uint16);
	~moFrame();

	void SetPos(int, int);
	void SetColor(Color);
	void Draw(SDL_Surface*);
	const int Width() const;
	const int Height() const;

	void SetSize(int, int);

	const SDL_Rect &GetRect();

private:
	SDL_Rect rect;
	SDL_Color cb, cd;
};

#endif // _MOFRAME_H
