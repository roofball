#include "moframe.h"
#include "defines.h"
#include "drawingtools.h"
#include <iostream>

moFrame::moFrame() {
	enabled = false;
	SetColor(MO_COLOR_DISABLED);
}

moFrame::moFrame(SDL_Rect &r) {
	rect = r;
	enabled = false;
	SetColor(MO_COLOR_DISABLED);
}

moFrame::moFrame(Sint16 x, Sint16 y, Uint16 w, Uint16 h) {
	SDL_Rect tmp = { x, y, w, h };
	rect = tmp;
	enabled = false;
	SetColor(MO_COLOR_DISABLED);
}

moFrame::~moFrame() {
}

void moFrame::SetPos(int x, int y) {
	rect.x = x;
	rect.y = y;
}

void moFrame::SetColor(Color c) {
	SDL_Color color(c);

	cb.r = color.r;
	cd.r = color.r * 2 / 3;
	cb.g = color.g;
	cd.g = color.g * 2 / 3;
	cb.b = color.b;
	cd.b = color.b * 2 / 3;
}

void moFrame::Draw(SDL_Surface *s) {
	SDL_LockSurface(s);
	DrawRect(s, rect.x+1, rect.y+1, rect.w-2, rect.h-2, cb);
	DrawRect(s, rect, cd);
	SDL_UnlockSurface(s);
}

const int moFrame::Width() const {
	return (rect.w);
}

const int moFrame::Height() const {
	return (rect.h);
}

void moFrame::SetSize(int w, int h) {
	rect.w = w;
	rect.h = h;
}

const SDL_Rect &moFrame::GetRect() {
	return (rect);
}
