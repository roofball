#ifndef MENUDATA_H
#define MENUDATA_H

#include "SDL.h"
#include "SDL_tbmf.h"

using namespace sdl_tbmf;

class MenuData {
public:
	MenuData(const char*);
	~MenuData();

	SDL_Surface
		*bg,
		*cb_box,
		*cb_check;

	Font
		*lFont,
		*mFont,
		*sFont;

	SDL_Color
		cSelected,
		cEnabled,
		cDisabled;

private:
};

#endif // MENUDATA_H
