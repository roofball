#ifndef MENUOBJECT_H
#define MENUOBJECT_H

#include "cchain.h"
#include "color.h"
#include "SDL.h"

class Menu;

class MenuObject {
public:
	MenuObject();
	virtual ~MenuObject();

	virtual void SetPos(int, int);
	virtual void SetColor(Color);
	virtual void Draw(SDL_Surface *);
	virtual const int Width() const;
	virtual const int Height() const;

	void SetFunct(void (*afunct)(MenuObject *, cChain<void *> *));
	void AddParm(void *);
	void RunFunct();

	bool Enabled() const;
	void SetEnabled(bool);

	bool Visible() const;
	void SetVisible(bool);

protected:
	bool enabled;
	bool visible;

	void (*afunct)(MenuObject *, cChain<void *> *);
	cChain<void *> *parms;
};

#endif // MENUOBJECT_H
