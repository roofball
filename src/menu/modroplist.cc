#include "modroplist.h"

#include "error.h"
#include "defines.h"
#include "drawingtools.h"

moDropList::moDropList(const MenuData *d, Font *f, Uint16 b):
 borderSize(b + 1),
 font(f),
 open(false) {
	rect.w = 30;
	rect.h = f->height() + 2 * borderSize;
	irect.w = rect.w - 2 * borderSize;
	irect.h = f->height();

	SetColor(MO_COLOR_ENABLED);
}

moDropList::~moDropList() {
	ListItem *curr;
	for (ulong i = 0; i < list.Size(); ++i) {
		curr = list.Next();
		delete (curr->ts);
		delete (curr);
	}
}

void moDropList::SetPos(int x, int y) {
	rect.x = x;
	rect.y = y;
	irect.x = x + borderSize;
	irect.y = y + borderSize;

	for (Uint32 i = 0; i < list.Size(); ++i)
		list.Next()->AutoPos(irect);
}

void moDropList::SetColor(Color c) {
	color = c;

	if (list.IsPolling())
		list.Current()->ts->setColor(c);
}

void moDropList::Draw(SDL_Surface *s) {
	SDL_FillRect(s, &irect, 0);

	if (list.IsPolling()) {
		SDL_Rect tmpRect(list.Current()->rect); tmpRect.y = irect.y;
		SDL_BlitSurface(list.Current()->ts->surface(), NULL, s, &tmpRect);
	}

	DrawBorders(s);
}

const int moDropList::Width() const {
	return (rect.w);
}

const int moDropList::Height() const {
	return (rect.h);
}

void moDropList::AddItem(const char *t, ItemAlign a) {
	ListItem *new_item = new ListItem();
	if (!new_item) throw Error(etNoMem);

	new_item->ts = new TextSurface(t, *font);
	if (!new_item->ts) throw Error(etNoMem);

	new_item->index = list.Size();

	new_item->align = a;

	new_item->rect.w = new_item->ts->surface()->w;
	new_item->rect.h = font->height();

	if (new_item->rect.w > irect.w) {
		irect.w = new_item->rect.w;
		rect.w = irect.w + 2 * borderSize;

		for (Uint32 i = 0; i < list.Size(); ++i)
			list.Next()->AutoPos(irect);
	}

	new_item->AutoPos(irect);
	new_item->ts->setColor((Color)MO_COLOR_ENABLED);

	list.PushBack(new_item);

	if (!list.IsPolling())
		list.First();
}

void moDropList::SetCurrent(const char *text) {
	if (strcmp(text, list.Current()->ts->text()) == 0)
		return;

	for (int i = 0; i < list.Size(); ++i)
		if (strcmp(text, list.Next()->ts->text()) == 0)
			return;

	throw Error("moDropList::SetCurrent: No such list item");
}

const char *moDropList::Browse(SDL_Surface *screen) {
	if (!list.Size())
		return (NULL);

	SDL_Rect openRect(rect), clipRect;
	SDL_Event event;
	SDL_Surface *tmpbg;
	open = true;

	openRect.h = 2 * borderSize + list.Size() * irect.h;

	// Catch background from under
	tmpbg = SDL_CreateRGBSurface(SDL_HWSURFACE, openRect.w, openRect.h, screen->format->BitsPerPixel, 0, 0, 0, 0);
	if (!tmpbg) throw Error(etNoMem);
	SDL_BlitSurface(screen, &openRect, tmpbg, NULL);

	SDL_GetClipRect(screen, &clipRect);
	SDL_SetClipRect(screen, NULL);

	selBef = list.Current();

	// The border area is untouched
	// during the Browse session
	DrawBorders(screen);

	while (open) {
		HandleEvents(event);
		UpdateGraph(screen);
	}

	// Restore background
	SDL_BlitSurface(tmpbg, NULL, screen, &openRect);
	SDL_FreeSurface(tmpbg);

	SDL_SetClipRect(screen, &clipRect);

	if (selBef != list.Current())
		return (list.Current()->ts->text());
	else
		return (NULL);
}

/* Starting private methods section */

void moDropList::HandleEvents(SDL_Event &event) {
	while (SDL_PollEvent(&event))
	if (event.type == SDL_KEYDOWN)
	switch (event.key.keysym.sym) {
	case SDLK_UP:
		SelNext();
		break;

	case SDLK_DOWN:
		SelPrev();
		break;

	case SDLK_ESCAPE:
		open = false;

		if (list.Current() != selBef) {
			list.Current()->ts->setColor((Color)MO_COLOR_ENABLED);
			selBef->ts->setColor((Color)MO_COLOR_SELECTED);

			while (list.Current() != selBef)
				list.Next();
		}
		break;

	case SDLK_RETURN:
		open = false;
		break;
	}
}

void moDropList::UpdateGraph(SDL_Surface *s) {
	ListItem *curr;
	for (Uint32 i = 0; i < list.Size(); ++i) {
		curr = list.Next();
		SDL_Rect bgRect(irect);
		bgRect.y += curr->index * irect.h;
		SDL_FillRect(s, &bgRect, 0);
		SDL_BlitSurface(curr->ts->surface(), NULL, s, &curr->rect);
	}

	if (list.Size())
		SDL_UpdateRect(s, rect.x, rect.y, rect.w + borderSize, 2 * borderSize + irect.h * list.Size());
	else
		SDL_UpdateRect(s, rect.x, rect.y, rect.w+1, rect.h);
}

void moDropList::DrawBorders(SDL_Surface *s) {
	SDL_LockSurface(s);
	if (open) {
		DrawRect(s, rect.x, rect.y, rect.w, irect.h*list.Size() + 2 * borderSize, color);
		for (Uint16 i = 1; i < borderSize; ++i)
			DrawRect(s, rect.x + i, rect.y + i, rect.w - 2 * i, irect.h * list.Size() + 2 * borderSize - 2 * i, 0);
	}
	else {
		DrawRect(s, rect.x, rect.y, rect.w, rect.h, color);
		for (Uint16 i = 1; i < borderSize; ++i)
			DrawRect(s, rect.x + i, rect.y + i, rect.w - 2 * i, rect.h - 2 * i, 0);
	}
	SDL_UnlockSurface(s);
}

void moDropList::SelNext() {
	list.Current()->ts->setColor((Color)MO_COLOR_ENABLED);
	list.Prev()->ts->setColor((Color)MO_COLOR_SELECTED);
}

void moDropList::SelPrev() {
	list.Current()->ts->setColor((Color)MO_COLOR_ENABLED);
	list.Next()->ts->setColor((Color)MO_COLOR_SELECTED);
}

void moDropList::ListItem::AutoPos(SDL_Rect ir) {
	switch (align) {
	case iaLeft:
		rect.x = ir.x;
		break;

	case iaCenter:
		rect.x = ir.x + (ir.w - rect.w) / 2;
		break;

	case iaRight:
		rect.x = ir.x + ir.w - rect.w;
		break;
	}

	rect.y = ir.y + index * ir.h;
}
