#ifndef MOCHECKBOX_H
#define MOCHECKBOX_H

#include "motext.h"
#include "menudata.h"

using namespace sdl_tbmf;

class moCheckBox: public MenuObject {
public:
	moCheckBox(MenuData const*, bool, Font*, const char*);
	~moCheckBox();

	void SetPos(int, int);
	void SetColor(Color);
	void Draw(SDL_Surface*);
	const int Width() const;
	const int Height() const;

	bool Checked() const;
	void SetChecked(bool);

private:
	bool checked;

	SDL_Rect cb_rect;
	SDL_Surface
		*graph_check,
		*graph_box;

	moText text;
};

#endif // MOCHECKBOX_H
