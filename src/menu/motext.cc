#include "motext.h"
#include "defines.h"

#include <iostream>

moText::moText(Font *f, const char *t, bool e):
 ts(t, *f) {
	trect.x = 0;
	trect.y = 0;
	trect.w = ts.surface()->w;
	trect.h = ts.surface()->h;

	if (e) SetColor(MO_COLOR_ENABLED);

	enabled = e;
}

moText::~moText() {
}

void moText::SetPos(int x, int y) {
	trect.x = x;
	trect.y = y;
}

void moText::SetColor(Color c) {
	ts.setColor(c);
}

void moText::Draw(SDL_Surface *t) {
	SDL_BlitSurface(ts, NULL, t, &trect);
}

const int moText::Width() const {
	return (ts.surface()->w);
}

const int moText::Height() const {
	return (ts.surface()->h);
}

void moText::SetText(const char *t) {
	ts.setText(t);
	trect.w = ts.surface()->w;
}

const char *moText::GetText() {
	return (ts.text());
}

SDL_Rect moText::GetRect() const {
	return (trect);
}
