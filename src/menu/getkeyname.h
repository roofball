#ifndef GETKEYNAME_H
#define GETKEYNAME_H

#include "SDL.h"

const char *GetKeyName(SDLKey);

#endif // GETKEYNAME_H
