#ifndef MENU_H
#define MENU_H

#include "game.h"
#include "enums.h"
#include "menudata.h"
#include "menupage.h"
#include "options.h"
#include "enums.h"
#include "SDL.h"

class Menu {
public:
	Menu(SDL_Surface *, const char *, Options *);
	~Menu();

	void Run(ResumeMode &);
	void SwitchPage(MenuPage *);

	void Init(Game &, ResumeMode &);

	void Undraw(SDL_Rect * = NULL);

private:
	MenuPage main;
	MenuData const *data;

	SDL_Surface *screen, *bg;
	Options *options;
	MenuPage *current;
	bool update;

	// used by menuinit
	MenuPage *InitOptions();
	MenuPage *InitPlayerOptions(PlayerOptions *, MenuPage *, SDL_Rect &);
	MenuPage *InitInfo();

	void HandleEvents(SDL_Event &, ResumeMode &);
};

#endif // MENU_H
