#include "optionsfile_errors.h"
#include <iostream>

using std::cerr;

void OFError::Print() const {
	cerr << "OptionsFile error:\n\t";
	switch (et) {
	case ofeUnknown:
		if (!es) {
			cerr << "Unknown error";
		}
		else cerr << es;
		break;

	case ofeTooLongString:
		cerr << "Too long string. Max string length is " << OF_STRMAXLEN - 1 << ".";
		break;

	case ofeWrongFormat:
		switch (num) {
		case 1:
			cerr << "outside section";
			break;

		case 2:
			cerr << "no section closing character";
			break;

		case 3:
			cerr << "no = character ";
			break;

		default:
			cerr << "unknown";
		}
		break;
	}
	cerr << "\n";
}

