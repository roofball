#include "fileexists.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

bool FileExists(const char *file) {
	int fd = open(file, O_RDONLY);
	if (fd > 0) {
		close(fd);
		return true;
	}
	else
		return false;
}
