#ifndef OPTIONSFILE_H
#define OPTIONSFILE_H

#include <fstream>
#include <cstring>
using namespace std;

typedef unsigned long long int UIntT;
typedef signed long long int SIntT;
typedef long double FloatT;
typedef const char *StringT;

class OptionsFile : private fstream {
	OptionsFile(const OptionsFile &);
	OptionsFile &operator=(const OptionsFile &);

public:
	OptionsFile(const char *file, bool load = true, bool saveOnDel = false);
	~OptionsFile();

	void Load();
	void Save();
	inline bool IsSaved() const;

	void Clear();
	bool IsClear() const;

	void SetUInt(const char *sect, const char *key, UIntT val);
	void SetUIntHex(const char *sect, const char *key, UIntT val);
	void SetSInt(const char *sect, const char *key, SIntT val);
	void SetFloat(const char *sect, const char *key, FloatT val);
	void SetString(const char *sect, const char *key, StringT ptr);

	UIntT GetUInt(const char *sect, const char *key) const;
	SIntT GetSInt(const char *sect, const char *key) const;
	FloatT GetFloat(const char *sect, const char *key) const;
	StringT GetString(const char *sect, const char *key) const;

	void Unset(const char *sect, const char *key);
	bool Has(const char *sect, const char *key) const { return (GetString(sect, key) != NULL); }

private:
	class StrList {
		StrList(const StrList &);
		StrList &operator=(const StrList &);

	public:
		StrList();
		~StrList();

		void Clear();
		bool IsClear() const;

		void Set(const char *key, const char *val);
		char *Get(const char *key) const;
		void Unset(const char *key);

		void StartPoll();
		int PollNext(char **, char **);

	private:
		struct Node {
			char *keyword;
			char *value;
			Node *next;
		} *head, *cur;
	} opts;
	char *fileName;
	bool autoSave, isSaved;

	void Set(const char *sect, const char *key, const char *val);
	char *Get(const char *sect, const char *key) const;
	char *CreateKeyword(const char *sect, const char *key) const;
	void SeparateKeyword(const char *keyword, char **sect, char **key) const;
	int NextSignificant();
	void AddChar(char *, char, size_t);
	void ParseString(char *, size_t);
};

bool OptionsFile::IsSaved() const {
	return isSaved;
}

#endif // OPTIONSFILE_H
