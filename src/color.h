#ifndef RB_COLOR_H
#define RB_COLOR_H

#include "SDL.h"

class Color {
public:
	Color(Uint32 = 0);
	Color(SDL_Color);
	~Color() {}

	inline operator Uint32() const;
	inline operator SDL_Color() const;

private:
	Uint32 color;
};

Color::operator Uint32() const {
	return color;
}

Color::operator SDL_Color() const {
	SDL_Color r;

#if SDL_ENDIANESS == SDL_LIL_ENDIAN
	r.unused = (color >> 24) & 0xff;
	r.b = (color >> 16) & 0xff;
	r.g = (color >> 8) & 0xff;
	r.r = color & 0xff;
#else
	r.unused = (color >> 24) & 0xff;
	r.r = (color >> 16) & 0xff;
	r.g = (color >> 8) & 0xff;
	r.b = color & 0xff;
#endif

	return r;
}

#endif // RB_COLOR_H
