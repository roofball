#ifndef DEBUG_H
#define DEBUG_H

#ifdef DEBUG

#if DEBUG > 0
#include <iostream>
#define DPRINT1(x) cout << #x << endl;
#else
#define DPRINT1(x)
#endif // DEBUG > 0

#if DEBUG > 1
#define DPRINT2(x) cout << #x << endl;
#else
#define DPRINT2(x)
#endif // DEBUG > 1

#if DEBUG > 2
#define DPRINT3(x) cout << #x << endl;
#else
#define DPRINT3(x)
#endif // DEBUG > 2

#if DEBUG > 3
#define DPRINT4(x) cout << #x << endl;
#else
#define DPRINT4(x)
#endif // DEBUG > 3

#if DEBUG > 4
#define DPRINT5(x) cout << #x << endl;
#else
#define DPRINT5(x)
#endif // DEBUG > 4

#else // DEBUG

#define DPRINT1(x)
#define DPRINT2(x)
#define DPRINT3(x)
#define DPRINT4(x)
#define DPRINT5(x)

#endif // DEBUG

#endif // DEBUG_H
