#ifndef CCHAIN_H
#define CCHAIN_H

#include "chainerror.h"

typedef unsigned long ulong;

#ifndef NULL
#define NULL 0
#endif

template <class T>
class cChain {
public:
	cChain();
	~cChain();

	ulong PushFront(T);
	ulong PushBack(T);
	ulong PopFront();
	ulong PopBack();

	ulong Size() const;

	void NewPoll();
	bool IsPolling();

	T &First();
	T &Last();
	T &Next();
	T &Prev();

	T &Current();

	T &operator=(T);
	operator T&();
private:
	template <class D>
	struct Node {
		D *data;
		Node *next;
		Node *prev;
	};

	Node<T> *head, *current;
	ulong size;
};

template <class T>
cChain<T>::cChain():
 head(new Node<T>),
 current(head),
 size(0) {
	if (!head) throw ceNoMem;
	head->data = NULL;
	head->next = NULL;
	head->prev = NULL;
}

template <class T>
cChain<T>::~cChain() {
	if (!size) {
		delete head;
		return;
	}

	current = head->next;
	head->prev->next = NULL;
	Node<T> *temp;

	while (current) {
		temp = current;
		current = current->next;
		delete temp->data;
		delete temp;
	}

	delete head;
}

template <class T>
ulong cChain<T>::PushFront(T d) {
	Node<T> *old_first = head->next,
		*new_first = new Node<T>;

	if (!new_first) throw ceNoMem;
	new_first->data = new T(d);
	if (!new_first->data) throw ceNoMem;

	head->next = new_first;	

	if (size) {
		head->prev->next = new_first;
		new_first->prev = head->prev;
		new_first->next = old_first;
		old_first->prev = new_first;
	}
	else {
		head->prev = new_first;
		new_first->next = new_first;
		new_first->prev = new_first;
	}

	return (++size);
}

template <class T>
ulong cChain<T>::PushBack(T d) {
	Node<T> *old_last = head->prev,
		*new_last = new Node<T>;

	if (!new_last) throw ceNoMem;
	new_last->data = new T(d);
	if (!new_last->data) throw ceNoMem;

	head->prev = new_last;

	if (size) {
		head->next->prev = new_last;
		new_last->next = head->next;
		new_last->prev = old_last;
		old_last->next = new_last;
	}
	else {
		head->next = new_last;
		new_last->next = new_last;
		new_last->prev = new_last;
	}

	return (++size);
}

template <class T>
ulong cChain<T>::PopFront() {
	if (!size) throw ceNoSuchNode;
	Node<T> *old_first = head->next,
		*new_first = head->next->next;

	new_first->prev = head->prev;
	head->prev->next = new_first;
	head->next = new_first;

	if (size == 1) {
		head->next = NULL;
		head->prev = NULL;
	}

	delete old_first->data;
	delete old_first;

	return (--size);
}

template <class T>
ulong cChain<T>::PopBack() {
	if (!size) throw ceNoSuchNode;
	Node<T> *old_last = head->prev,
		*new_last = head->prev->prev;

	new_last->next = head->next;
	head->next->prev = new_last;
	head->prev = new_last;

	if (size == 1) {
		head->next = NULL;
		head->prev = NULL;
	}

	delete old_last->data;
	delete old_last;

	return (--size);
}

template <class T>
ulong cChain<T>::Size() const {
	return size;
}

template <class T>
void cChain<T>::NewPoll() {
	current = head;
}

template <class T>
bool cChain<T>::IsPolling() {
	return (current != head);
}

template <class T>
T &cChain<T>::First() {
	if (!size) throw ceNoSuchNode;
	current = head->next;
	return (*current->data);
}

template <class T>
T &cChain<T>::Last() {
	if (!size) throw ceNoSuchNode;
	current = head->prev;
	return (*current->data);
}

template <class T>
T &cChain<T>::Next() {
	if (!size) throw ceNoSuchNode;
	current = current->next;
	return (*current->data);
}

template <class T>
T &cChain<T>::Prev() {
	if (!size) throw ceNoSuchNode;
	current = current->prev;
	return (*current->data);
}

template <class T>
T &cChain<T>::Current() {
	if (!current->data) throw ceNoSuchNode;
	return (*current->data);
}

template <class T>
T &cChain<T>::operator=(T d) {
	if (!current->data) throw ceNoSuchNode;
	return (*current->data = d);
}

template <class T>
cChain<T>::operator T&() {
	if (!current->data) throw ceNoSuchNode;
	return (*current->data);
}

#endif // CCHAIN_H
