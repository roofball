#ifndef ERROR_H
#define ERROR_H

#include <string>
#include <iostream>

using std::string;
using std::cerr;
using std::endl;

enum ErrorType {
	etSuccess, etNoError = 0, etNone = 0, // No errors
	etFailure, etUnknown = 1, // Unknown/unspecified error
	etNoMem,
	etFileOpen,	
	etFileFormat,
	etFileRead,
	etSurfaceCreate,
	etSurfaceLock,
	etImageLoad,
	etAccessEmptyList,
	etNoSuchKey,
	etNumberOfErrors
};
const string errorStrings[etNumberOfErrors] = {
	"No error",
	"Unknown error",
	"Out of memory",
	"File not found or permission denied",
	"File format was invalid",
	"Could not read file",
	"Could not create surface",
	"Could not lock surface",
	"Failed to load image",
	"Tried to access empty list",
	"Cannot find a key from list"
};

class Error {
public:
	Error():
	 errorType(etUnknown), errorString("") {}
	Error(const Error &e) {
		errorType = e.errorType;
		errorString = e.errorString;
	}
	Error(ErrorType e):
	 errorType(e), errorString("") {}
	Error(const string &s):
	 errorType(etUnknown), errorString(s) {}
	Error(ErrorType e, const string &s):
	 errorType(e), errorString(s) {}
	~Error() {}

	bool operator==(const Error &e) {
		return (errorType == e.errorType);
	}
	void PrintError() const {
		cerr << " type: " << errorStrings[errorType] << endl;
		if (errorString != "") {
			cerr << " info: " << errorString << endl;
		}
		else cerr << endl;
	}
	ErrorType GetErrorType() const {
		return errorType;
	}
	string GetErrorString() const {
		return errorString;
	}

private:
	ErrorType errorType;
	string errorString;
};

#endif // ERROR_H
