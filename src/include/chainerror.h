#ifndef CHAINERROR_H
#define CHAINERROR_H

enum ChainError {
	ceNoMem,
	ceNoSuchNode
};

#endif // CHAINERROR_H
