#ifndef DEFINES_H
#define DEFINES_H

#define SCR_RESO_X 320
#define SCR_RESO_Y 240
#define SCR_BPP 32

#define MOVE_SPEED 4

#define ALPHA_COLOR 0x00ff00
#define GROUND_LEVEL 202

#define PL1_X 222
#define PL2_X 98
#define LFOOT_OFFSET_X 3
#define LFOOT_OFFSET_Y 51
#define RFOOT_OFFSET_X 59
#define RFOOT_OFFSET_Y 51
#define HEAD_OFFSET_X 31
#define HEAD_OFFSET_Y 6
#define BALL_OFFSET 9
#define BALL_DIAMETER 19
#define AIMCURSOR_SIZE 7
#define AIMCURSOR_OFFSET 3

#define PB_BORDER_COLOR 0xffff00
#define PB_BG_COLOR 0x000000
#define PB_BORDER_DIST 3
#define PB_W 114
#define PB_H 10

#define MO_COLOR_SELECTED 0xff7f00
#define MO_COLOR_ENABLED  0x954500
#define MO_COLOR_DISABLED 0x5f5f5f

/*
#define MENUOBJECT_COLOR_SELECTED { 0xff, 0x7f, 0x00 }
#define MENUOBJECT_COLOR_ENABLED  { 0x95, 0x45, 0x00 }
#define MENUOBJECT_COLOR_ENABLED  { 0x8c, 0x41, 0x00 }
#define MENUOBJECT_COLOR_DISABLED { 0x5f, 0x5f, 0x5f }
*/

#define IMGDIR "images/"
#define FNTDIR "fonts/"

#define PATHSIZE 2048

// use mouse to get coords in menu mode
//#define MENUDEVEL

#endif // DEFINES_H
