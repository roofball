#ifndef CHAIN_H
#define CHAIN_H

#include "chainerror.h"

typedef unsigned long ulong;

#ifndef NULL
#define NULL 0
#endif

template <class T>
class Chain {
public:
	Chain();
	Chain(const Chain &);
	~Chain();

	ulong PushFront(T);
	ulong PushBack(T);
	ulong PopFront();
	ulong PopBack();

	ulong Size() const;
	void Clear();

	void NewPoll();
	bool PollNext();
	bool PollPrev();

	T &First();
	T &Last();
	T &Next();
	T &Prev();
	T &Current();

	ulong Insert(T);
	void Replace(T);
	ulong Remove();

	ulong Insert(ulong, T);
	void Replace(ulong, T);
	ulong Remove(ulong);

	void SetCurrent(ulong);

	Chain &operator=(const Chain &);
	T &operator[](ulong);
	T &operator=(T);
	operator T&();

private:
	template <class D>
	struct Node {
		D *data;
		Node *next;
		Node *prev;
	};

	Node<T> *GetNode(ulong);

	Node<T> *head, *current;
	ulong size;
};

template <class T>
Chain<T>::Chain():
 head(new Node<T>),
 current(head),
 size(0) {
	if (!head) throw ceNoMem;
	head->data = NULL;
	head->next = head;
	head->prev = head;
}

template <class T>
Chain<T>::Chain(const Chain &c):
 head(new Node<T>) {
	if (!head) throw ceNoMem;
	head->data = NULL;
	head->next = head;
	head->prev = head;

	operator=(c);
}

template <class T>
Chain<T>::~Chain() {
	Clear();

	delete head;
}

template <class T>
ulong Chain<T>::PushFront(T d) {
	Node<T> *old_first = head->next,
		*new_first = new Node<T>;

	if (!new_first) throw ceNoMem;
	new_first->data = new T(d);
	if (!new_first->data) throw ceNoMem;

	head->next = new_first;
	new_first->prev = head;
	new_first->next = old_first;
	old_first->prev = new_first;

	return (++size);
}

template <class T>
ulong Chain<T>::PushBack(T d) {
	Node<T> *old_last = head->prev,
		*new_last = new Node<T>;

	if (!new_last) throw ceNoMem;
	new_last->data = new T(d);
	if (!new_last->data) throw ceNoMem;

	head->prev = new_last;
	new_last->next = head;
	new_last->prev = old_last;
	old_last->next = new_last;

	return (++size);
}

template <class T>
ulong Chain<T>::PopFront() {
	if (!size) return 0;
	Node<T> *old_first = head->next,
		*new_first = head->next->next;

	head->next = new_first;
	new_first->prev = head;
	delete old_first->data;
	delete old_first;

	return (--size);
}

template <class T>
ulong Chain<T>::PopBack() {
	if (!size) return 0;
	Node<T> *old_last = head->prev,
		*new_last = head->prev->prev;

	head->prev = new_last;
	new_last->next = head;
	delete old_last->data;
	delete old_last;

	return (--size);
}

template <class T>
ulong Chain<T>::Size() const {
	return size;
}

template <class T>
void Chain<T>::Clear() {
	current = head->next;
	Node<T> *temp;

	while (current->data) {
		temp = current;
		current = current->next;
		delete temp->data;
		delete temp;
	}
}

template <class T>
void Chain<T>::NewPoll() {
	current = head;
}

template <class T>
bool Chain<T>::PollNext() {
	current = current->next;
	return (current->data);
}

template <class T>
bool Chain<T>::PollPrev() {
	current = current->prev;
	return (current->data);
}

template <class T>
T &Chain<T>::First() {
	current = head->prev;
	if (!current->data) throw ceNoSuchNode;
	return (*current->data);
}

template <class T>
T &Chain<T>::Last() {
	current = head->prev;
	if (!current->data) throw ceNoSuchNode;
	return (*current->data);
}

template <class T>
T &Chain<T>::Next() {
	current = current->next;
	if (!current->data) throw ceNoSuchNode;
	return (*current->data);
}

template <class T>
T &Chain<T>::Prev() {
	current = current->prev;
	if (!current->data) throw ceNoSuchNode;
	return (*current->data);
}

template <class T>
T &Chain<T>::Current() {
	if (!current->data) throw ceNoSuchNode;
	return (*current->data);
}

template <class T>
ulong Chain<T>::Insert(T d) {
	if (!current->data) throw ceNoSuchNode;
	Node<T> *old_current = current,
		*new_current = new Node<T>;

	if (!new_current) throw ceNoMem;
	new_current->data = new T(d);
	if (!new_current->data) throw ceNoMem;

	old_current->prev->next = new_current;
	new_current->prev = old_current->prev;
	new_current->next = old_current;
	old_current->prev = new_current;

	current = new_current;
	return (++size);
}

template <class T>
void Chain<T>::Replace(T d) {
	if (!current->data) throw ceNoSuchNode;
	*current->data = d;
}

template <class T>
ulong Chain<T>::Remove() {
	if (!current->data) throw ceNoSuchNode;
	Node<T> *temp;
	if (current->next->data)
		temp = current->next;
	else	temp = current->prev;

	current->prev->next = current->next;
	current->next->prev = current->prev;
	delete current->data;
	delete current;

	current = temp;
	return (--size);
}

template <class T>
ulong Chain<T>::Insert(ulong i, T d) {
	Node<T> *old_temp = GetNode(i),
		*new_temp = new Node<T>;

	if (!new_temp) throw ceNoMem;
	new_temp->data = new T(d);
	if (!new_temp->data) throw ceNoMem;

	old_temp->prev->next = new_temp;
	new_temp->prev = old_temp->prev;
	new_temp->next = old_temp;
	old_temp->prev = new_temp;

	return (++size);
}

template <class T>
void Chain<T>::Replace(ulong i, T d) {
	*GetNode(i)->data = d;
}

template <class T>
ulong Chain<T>::Remove(ulong i) {
	Node<T> *temp = GetNode(i);
	if (temp == current) {
		if (current->next->data)
			current = current->next;
		else
			current = current->prev;
	}

	temp->prev->next = temp->next;
	temp->next->prev = temp->prev;
	delete temp->data;
	delete temp;

	return (--size);
}

template <class T>
void Chain<T>::SetCurrent(ulong i) {
	current = GetNode(i);
}

template <class T>
Chain<T> &Chain<T>::operator=(const Chain &c) {
	if (&c == this)
		return *this;

	Clear();

	Node<T> *temp = c.head->next;
	Node<T> *old_last, *new_last;

	while (temp->data) {
		old_last = head->prev;
		new_last = new Node<T>;
		if (!new_last) throw ceNoMem;
		new_last->data = new T(*temp->data);
		if (!new_last->data) throw ceNoMem;

		head->prev = new_last;
		new_last->next = head;
		new_last->prev = old_last;
		old_last->next = new_last;

		if (temp == c.current)
			current = new_last;
		temp = temp->next;
	}

	size = c.size;

	return *this;
}

template <class T>
T &Chain<T>::operator[](ulong i) {
	return (*GetNode(i)->data);
}

template <class T>
T &Chain<T>::operator=(T d) {
	if (!current->data) throw ceNoSuchNode;
	return (*current->data = d);
}

template <class T>
Chain<T>::operator T&() {
	if (!current->data) throw ceNoSuchNode;
	return (*current->data);
}

template <class T>
Chain<T>::Node<T> *Chain<T>::GetNode(ulong i) {
	if (!(i < size)) throw ceNoSuchNode;
	Node<T> *temp;
	ulong j;

	if (i < size / 2) {
		temp = head->next;
		for (j = 0; j < i; ++j)
			temp = temp->next;
	}
	else {
		temp = head->prev;
		for (j = size - 1; j > i; --j)
			temp = temp->prev;
	}

	return (temp);
}

#endif // CHAIN_H
