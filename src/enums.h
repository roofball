#ifndef ENUMS_H
#define ENUMS_H

enum AimMode { amNever, amOnKick, amAlways };
enum ResumeMode { rmExit = 0, rmQuit = 0, rmMenu, rmGame };

#endif // ENUMS_H
