#include "signalfs.h"
#include "options.h"
#include "menupage.h"
#include "getkeyname.h"
#include "enums.h"
#include "game.h"
#include "menu.h"

#include "mocheckbox.h"
#include "modroplist.h"
#include "moframe.h"

void Action_SwitchPage(MenuObject *mo, cChain<void *> *parms) {
	Menu *menu;
	MenuPage *page;

	menu = (Menu *)parms->First();
	page = (MenuPage *)parms->Next();

	menu->SwitchPage(page);
}

void Action_GrabKey(MenuObject *mo, cChain<void *> *parms) {
	Menu *menu;
	SDLKey *key;
	SDL_Surface *screen;

	menu = (Menu *)parms->First();
	key = (SDLKey *)parms->Next();
	screen = (SDL_Surface *)parms->Next();

	moText *object;
	SDL_Rect rect;

	object = (moText *)mo;
	rect = object->GetRect();

	char text[128];
	strcpy(text, object->GetText());

	// Display waiting graph
	menu->Undraw(&rect);
	object->SetText("_");
	object->Draw(screen);
	if (rect.w < object->GetRect().w)
		rect.w = object->GetRect().w;
	SDL_UpdateRects(screen, 1, &rect);
	menu->Undraw(&rect);

	bool wait(true);
	SDL_Event event;

	while (wait) {
		while (SDL_PollEvent(&event))
		if (event.type == SDL_KEYDOWN)
		switch (event.key.keysym.sym) {
		case SDLK_ESCAPE:
			object->SetText(text);
			object->Draw(screen);
			wait = false;
			break;

		default:
			*key = event.key.keysym.sym;
			object->SetText(GetKeyName(*key));
			object->Draw(screen);
			wait = false;
			break;
		}			
	}

	if (rect.w < object->GetRect().w)
		rect.w = object->GetRect().w;
	SDL_UpdateRects(screen, 1, &rect);
}

void Action_ResumeGame(MenuObject *mo, cChain<void *> *parms) {
	*(ResumeMode *)parms->First() = rmGame;
}

void Action_NewGame(MenuObject *mo, cChain<void *> *parms) {
	Game *game;
	ResumeMode *mode;
	MenuObject *object;

	game = (Game *)parms->First();
	mode = (ResumeMode *)parms->Next();
	object = (MenuObject *)parms->Next();

	object->SetEnabled(true);
	game->Reset();
	*mode = rmGame;
}

void Action_Exit(MenuObject *mo, cChain<void *> *parms) {
	*(ResumeMode *)parms->First() = rmExit;
}

void Action_ToggleFS(MenuObject *mo, cChain<void *> *parms) {
	Menu *menu;
	Options *options;
	SDL_Surface *screen;
	bool *update;

	menu = (Menu *)parms->First();
	options = (Options *)parms->Next();
	screen = (SDL_Surface *)parms->Next();
	update = (bool *)parms->Next();

	moCheckBox *object;
	bool checked, isFS;

	object = (moCheckBox *)mo;
	checked = !object->Checked();
	object->SetChecked(checked);

	isFS = (screen->flags & SDL_FULLSCREEN ? true : false);

	if (checked != isFS)
		SDL_WM_ToggleFullScreen(screen);

	options->fullScreen = checked;
	*update = true;
}

void Action_BrowseStyle(MenuObject *mo, cChain<void *> *parms) {
	Menu *menu;
	PlayerOptions *pOpt;
	SDL_Surface *s;
	bool *update;

	menu = (Menu *)parms->First();
	pOpt = (PlayerOptions *)parms->Next();
	s = (SDL_Surface *)parms->Next();
	update = (bool *)parms->Next();

	const char *sel = ((moDropList *)mo)->Browse(s);
	if (sel) {
		pOpt->SetStyle(sel);
		pOpt->update = true;
	}

	*update = true;
}

void Action_BrowseKickCursor(MenuObject *mo, cChain<void *> *parms) {
	Menu *menu;
	PlayerOptions *pOpt;
	SDL_Surface *s;
	bool *update;

	menu = (Menu *)parms->First();
	pOpt = (PlayerOptions *)parms->Next();
	s = (SDL_Surface *)parms->Next();
	update = (bool *)parms->Next();

	const char *sel = ((moDropList *)mo)->Browse(s);
	if (sel) {
		if (strcmp("Always", sel) == 0)
			pOpt->aim = amAlways;
		else if (strcmp("OnKick", sel) == 0)
			pOpt->aim = amOnKick;
		else if (strcmp("Never", sel) == 0)
			pOpt->aim = amNever;
	}

	*update = true;
}
