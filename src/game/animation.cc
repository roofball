#include "animation.h"

Animation::Animation() {
	SetRate(1, 1);
	d = -1;
	GoHead();
	framesPolled = 0;
	width = 0;
	height = 0;
}

Animation::~Animation() {
}

void Animation::Clear() {
	SetRate(1, 1);
	d = -1;
	CircleChain::Clear();
	GoHead();
	framesPolled = 0;
	width = 0;
	height = 0;
}

void Animation::AddFrame(SDL_Surface *s) {
	if (!Size()) {
		width = s->w;
		height = s->h;
	}
	else {
		if (width != s->w || height != s->h)
			throw Error("Animation::AddFrame: Frame sizes can't vary");
	}

	Push(s);
}

SDL_Surface *Animation::RemoveLastFrame() {
	if (Size() == 1) {
		width = 0;
		height = 0;
	}
	return Pop();
}

void Animation::ReplaceFrame(SDL_Surface *s) {
	if (!Size()) {
		width = s->w;
		height = s->h;
	}
	else {
		if (width != s->w || height != s->h)
			throw Error("Animation::AddFrame: Frame sizes can't vary");
	}

	Replace(s);
}

void Animation::SetRate(int go, int gn) {
	// Without this deadlock is possible in PollFrame()
	if (gn == 0) go = 0;

	o = (go < 0) ? -go : go;
	n = (gn < 0) ? -gn : gn;

	if (go * gn == 0)
		dir = 0;
	else
		dir = (go * gn < 0) ? -1 : 1;
	d = -n;
}

void Animation::SetSpeed(long double l) {
	int saveD = d;
	SetRate(int(l * 256), 256);
	d = saveD;
}

void Animation::Restart() {
	SetRate(dir * o, n);
	GoHead();
	framesPolled = 0;
}

void Animation::Reverse() {
	dir *= -1;
}

SDL_Surface *Animation::PollFrame() {
	uint poll = 0;

	d += o;
	if (d >= 0) {
		d -= n;
		++poll;
	}
	if (o > n) { // Skip frames in animation
		while (d >= 0) {
			d -= n;
			++poll;
		}
	}

	GoForward(poll * dir);
	framesPolled += poll;

	return Current();
}

SDL_Surface *Animation::FirstFrame() {
	framesPolled = 0;
	return First();
}

SDL_Surface *Animation::LastFrame() {
	framesPolled = Size() - 1;
	First();
	return Prev();
}

bool Animation::HasLooped() {
	return (framesPolled >= Size());
}

void Animation::SkipFrame() {
	Next();
}

void Animation::SkipFrameBack() {
	Prev();
}

SDL_Surface *Animation::GetFrame() {
	return Current();
}

uint Animation::FrameCount() const {
	return Size();
}

AnimDir Animation::Dir() const {
	switch (dir) {
	case -1:
		return adBackward;

	case 0:
		return adPaused;

	case 1:
		return adForward;

	default:
		throw Error("Invalid animation direction!");
	}
}

Uint16 Animation::Width() const {
	return width;
}

Uint16 Animation::Height() const {
	return height;
}
