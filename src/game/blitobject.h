#ifndef BLITOBJECT_H
#define BLITOBJECT_H

#include "gamedata.h"
#include "animation.h"
#include "SDL.h"

class BlitObject {
public:
	BlitObject(SDL_Surface *, GameData *);
	virtual ~BlitObject() {}

	void Draw();
	void Undraw();
	void PollFrame();

	void Move(Sint16, Sint16);
	void MoveX(Sint16);
	void MoveY(Sint16);
	void SetPos(Sint16, Sint16);

	void SetLoopAnim(Animation *);
	void PlayAnimOnce(Animation *);

	Uint16 Width();
	Uint16 Height();
	Sint16 X() const;
	Sint16 Y() const;

protected:
	GameData *data;

private:
	SDL_Rect rect, rect2;
	Animation *loop, *once;
	SDL_Surface *screen;

	Animation *CurrentAnim();
};

#endif // BLITOBJECT_H
