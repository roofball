#ifndef BALL_H
#define BALL_H

#include "kicktype.h"
#include "blitobject.h"
#include "SDL.h"

class Ball : public BlitObject {
public:
	Ball(SDL_Surface *, GameData *);
	~Ball();

	void Reset();

	void LaunchNew();

	int Update();

	int Kick(KickType, int power, Sint16 x, Sint16 y);

	void SetSpin(long double);
	long double GetSpin() const;

private:
	Animation anim;
	long double spin, vx, vy, x, y;
	long long updates;
};

#endif // BALL_H
