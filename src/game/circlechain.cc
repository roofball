#include "circlechain.h"

CircleChain::CircleChain():
 size(0) {
	head.data = NULL;
	head.next = &head;
	head.prev = &head;
	current = &head;
}

CircleChain::~CircleChain() {
	Clear();
}

void CircleChain::Clear() {
	Node *tmp = head.next;

	while (size--) {
		current = tmp->next;
		delete tmp;
		tmp = current;
	}
	current = head.prev = head.next = &head;
	size = 0;
}

uint CircleChain::Push(SDL_Surface *d) {
	Node *newNode = new Node;
	if (!newNode) throw;
	newNode->data = d;

	if (size) {
		newNode->next = head.next;
		newNode->prev = head.prev;
		head.prev->next = newNode;
		head.prev = newNode;
		head.next->prev = newNode;
	}
	else {
		newNode->next = newNode->prev = newNode;
		head.next = newNode;
		head.prev = newNode;
		current = newNode;
	}

	return ++size;
}

SDL_Surface *CircleChain::Pop() {
	if (size) {
		Node *delNode = head.prev;
		SDL_Surface *tmpData = delNode->data;
		delNode->prev->next = head.next;
		head.prev = delNode->prev;
		head.next->prev = delNode->prev;

		--size;
		delete delNode;

		return tmpData;
	}
	else throw Error(etAccessEmptyList);
}

void CircleChain::Replace(SDL_Surface *d) {
	if (!size) {
		Push(d);
		return;
	}

	current->data = d;
}

SDL_Surface *CircleChain::GoForward(int i) {
	if (i > 0)
		while(i--) current = current->next;
	else if (i < 0)
		while(i++) current = current->prev;

	return current->data;
}
