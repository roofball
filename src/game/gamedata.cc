#include "error.h"
#include "gamedata.h"
#include "defines.h"
#include "getgraphic.h"
#include "fileexists.h"
#include <cstring>
#include <iostream>
#include <string>

using namespace sdl_tbmf;

GameData::GameData(const char *prefix) {
	Uint32 i;

	string imgpath = prefix , fntpath = prefix, imgfile;
	imgpath += IMGDIR;
	fntpath += FNTDIR;

	imgfile = imgpath + "gamebg.bmp";
	GetGraphic(&bg, imgfile.c_str());

	SDL_Surface *s;

	for (int j = 0; j < 10; ++j) {
		for (i = 0; i < 10; ++i) {
			imgfile = imgpath + "aimcursor" + char('0' + j) + "_" + char('0' + i) + ".bmp";
			if (!FileExists(imgfile.c_str())) break;
			GetGraphic(&s, imgfile.c_str());
			if (s->w != AIMCURSOR_SIZE || s->h != AIMCURSOR_SIZE)
				throw Error("Aimcursor size should be 7x7");
			aimcursors[j].PushBack(s);
		}
	}

	for (i = 0; i < 16; ++i) {
		imgfile = imgpath + "ball" + char('0' + i/10) + char('0' + i%10) + ".bmp";
		GetGraphic(&s, imgfile.c_str());
		ball.PushBack(s);
	}

	for (i = 0; i < 3; ++i) {
		imgfile = imgpath + "neighbor" + char('0' + i) + ".bmp";
		GetGraphic(&s, imgfile.c_str());
		neighbor.PushBack(s);
	}


	imgfile = imgpath + "player_walk0.bmp";
	GetGraphic(&s, imgfile.c_str());
	player_stand.PushBack(s);
	for (i = 0; i < 4; ++i) {
		imgfile = imgpath + "player_walk" + char('0' + i) + ".bmp";
		GetGraphic(&s, imgfile.c_str());
		player_walk.PushBack(s);
	}


	imgfile = imgpath + "player_kh0.bmp";
	GetGraphic(&s, imgfile.c_str());
	player_kh.PushBack(s);

	imgfile = imgpath + "player_kl0.bmp";
	GetGraphic(&s, imgfile.c_str());
	player_kl.PushBack(s);

	imgfile = imgpath + "player_kr0.bmp";
	GetGraphic(&s, imgfile.c_str());
	player_kr.PushBack(s);

//	Font *font;
}

GameData::~GameData() {
	ball.NewPoll();
	while(ball.PollNext())
		SDL_FreeSurface(ball);

	SDL_FreeSurface(bg);

	for (int i = 0; i < 10; ++i) {
		aimcursors[i].NewPoll();
		while (aimcursors[i].PollNext())
			SDL_FreeSurface(aimcursors[i].Current());
	}

	neighbor.NewPoll();
	while(neighbor.PollNext())
		SDL_FreeSurface(neighbor);

	player_stand.NewPoll();
	while(player_stand.PollNext())
		SDL_FreeSurface(player_stand);

	player_walk.NewPoll();
	while(player_walk.PollNext())
		SDL_FreeSurface(player_walk);

	player_kh.NewPoll();
	while(player_kh.PollNext())
		SDL_FreeSurface(player_kh);

	player_kl.NewPoll();
	while(player_kl.PollNext())
		SDL_FreeSurface(player_kl);

	player_kr.NewPoll();
	while(player_kr.PollNext())
		SDL_FreeSurface(player_kr);

	//delete font;
}
