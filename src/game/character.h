#ifndef CHARACTER_H
#define CHARACTER_H

#include "blitobject.h"
#include "aimcursor.h"
#include "charactercolors.h"
#include "kicktype.h"
#include "SDL.h"

class Character : public BlitObject {
public:
	Character(SDL_Surface *, GameData *);
	~Character() {}

	void Walk(WalkDir);
	void Kick(KickType);

	const CharacterColors &GetColors() const;
	void SetColors(const CharacterColors &);

private:
	Animation stand, walk, kl, kr, kh;
	CharacterColors colors;
};

#endif // CHARACTER_H
