#include "aimcursor.h"

AimCursor::AimCursor(SDL_Surface *s, GameData *gd):
 BlitObject(s, gd) {
	for (unsigned int i = 0; i < data->aimcursors[0].Size(); ++i)
		anim.AddFrame(data->aimcursors[0][i]);
	anim.SetRate(1, 2);
	SetLoopAnim(&anim);
}

AimCursor::~AimCursor() {
}

void AimCursor::SetCursor(unsigned int i) {
	if (data->aimcursors[i].Size()) {
		anim.Clear();
		for (unsigned int j = 0; j < data->aimcursors[i].Size(); ++j) {
			anim.AddFrame(data->aimcursors[i][j]);
		}
		anim.SetRate(1, 2);
	}
}
