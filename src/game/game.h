#ifndef GAME_H
#define GAME_H

#include "ball.h"
#include "player.h"
#include "stats.h"
#include "neighbor.h"
#include "gamedata.h"
#include "enums.h"
#include "options.h"
#include "playeroptions.h"
#include "SDL.h"

class Game {
public:
	Game(SDL_Surface *, const char *, Options *);
	~Game();

	void Run(ResumeMode &);
	void DrawBG();

	void Reset();

private:
	void HandleEvents(SDL_Event &, ResumeMode &);
	void HandlePlayerButtons(const SDLKey &, ResumeMode &, bool);

	GameData *data;
	Options *options;
	SDL_Surface *screen;

	Player pl1, pl2;
	Ball ball;
	Stats stats;
};

#endif // GAME_H
