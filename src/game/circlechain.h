#ifndef CIRCLECHAIN_H
#define CIRCLECHAIN_H

typedef unsigned int uint;

#include "SDL.h"
#include "error.h"

class CircleChain {
public:
	CircleChain();
	~CircleChain();

	void Clear();

	uint Push(SDL_Surface *);
	SDL_Surface *Pop();
	void Replace(SDL_Surface *);

	inline uint Size() const;

	inline void GoHead();
	inline SDL_Surface *First();
	inline SDL_Surface *Next();
	inline SDL_Surface *Prev();
	SDL_Surface *GoForward(int);
	inline SDL_Surface *GoBackward(int);
	inline SDL_Surface *Current() const;

	inline operator SDL_Surface *() const;

	inline SDL_Surface *operator++();
	inline SDL_Surface *operator--();
	inline SDL_Surface *operator++(int);
	inline SDL_Surface *operator--(int);
	inline SDL_Surface *operator+=(int);
	inline SDL_Surface *operator-=(int i);

private:
	struct Node {
		SDL_Surface *data;
		Node *next;
		Node *prev;
	};
	Node head, *current;
	uint size;
};

uint CircleChain::Size() const {
	return size;
}

void CircleChain::GoHead() {
	current = &head;
}

SDL_Surface *CircleChain::First() {
	current = head.next;
	return current->data;
}

SDL_Surface *CircleChain::Next() {
	current = current->next;
	return current->data;
}

SDL_Surface *CircleChain::Prev() {
	current = current->prev;
	return current->data;
}

SDL_Surface *CircleChain::GoBackward(int i) {
	return GoForward(-i);
}

SDL_Surface *CircleChain::Current() const {
	return current->data;
}

CircleChain::operator SDL_Surface *() const {
	return current->data;
}

SDL_Surface *CircleChain::operator++() {
	return Next();
}

SDL_Surface *CircleChain::operator--() {
	return Prev();
}

SDL_Surface *CircleChain::operator++(int) {
	Node *tmp = current;
	Next();
	return tmp->data;
}

SDL_Surface *CircleChain::operator--(int) {
	Node *tmp = current;
	Prev();
	return tmp->data;
}

SDL_Surface *CircleChain::operator+=(int i) {
	return GoForward(i);
}

SDL_Surface *CircleChain::operator-=(int i) {
	return GoForward(-i);
}

#endif // CIRCLECHAIN_H
