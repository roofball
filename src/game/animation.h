#ifndef ANIMATION_H
#define ANIMATION_H

#include "circlechain.h"
#include "SDL.h"

enum AnimDir { adBackward = -1, adPaused = 0, adForward = 1 };

class Animation : private CircleChain {
public:
	Animation();
	~Animation();

	void Clear();

	void AddFrame(SDL_Surface *);
	SDL_Surface *RemoveLastFrame();
	void ReplaceFrame(SDL_Surface *);

	void SetRate(int, int = 1);
	void SetSpeed(long double);

	void Restart();
	void Reverse();

	SDL_Surface *PollFrame();
	SDL_Surface *FirstFrame();
	SDL_Surface *LastFrame();
	bool HasLooped();
	void SkipFrame();
	void SkipFrameBack();
	SDL_Surface *GetFrame();
	uint FrameCount() const;
	AnimDir Dir() const;

	Uint16 Width() const;
	Uint16 Height() const;

private:
	uint o, n, framesPolled;
	int dir, d;
	Uint16 width, height;
};

#endif // ANIMATION_H
