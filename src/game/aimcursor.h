#ifndef	AIMCURSOR_H
#define AIMCURSOR_H

#include "enums.h"
#include "animation.h"
#include "blitobject.h"
#include "SDL.h"

class AimCursor : public BlitObject {
public:
	AimCursor(SDL_Surface *, GameData *);
	~AimCursor();

	void SetCursor(unsigned int);

private:
	Animation anim;
};

#endif // AIMCURSOR_H
