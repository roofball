#include "player.h"
#include "defines.h"
#include "error.h"

Player::Player(SDL_Surface *s, GameData *gd, PlayerOptions const *go, int num):
 opt(go),
 character(s, gd),
 aimcursor(s, gd),
 powerbar(s, gd, num),
 number(num),
 movement(0),
 aiming(opt->aim),
 drawCursor(false) {
// This is done when NewGame is called
//	Reset();
}

void Player::Reset() {
	character.Walk(wdNone);
	character.PollFrame();
	movement = 0;
	updates = 0;

	switch (number) {
	case 1:
		character.SetPos(PL1_X - character.Width() / 2, GROUND_LEVEL - character.Height());
		break;

	case 2:
		character.SetPos(PL2_X - character.Width() / 2, GROUND_LEVEL - character.Height());
		break;

	default:
		throw Error("Player::Reset(): Invalid player number requested");
	}

	aimcursor.SetCursor(0);
	powerbar.Reset();
}

void Player::Update() {
	aimcursor.PollFrame();
	powerbar.Update();
	if (++updates % 2) return;
	if (movement) {
		Sint16 nx = character.X() + movement;
		if ((nx >= -4 * MOVE_SPEED) && (nx <= SCR_RESO_X - character.Width() + 4 * MOVE_SPEED))
			character.MoveX(movement);
		//else {
		//	movement = 0;
		//	character.Walk(wdNone);
		//}
	}
	character.PollFrame();
}

void Player::UpdateGraph(const CharacterColors &c) {
	character.SetColors(c);
}

void Player::Draw() {
	character.Draw();
	powerbar.Draw();

	switch (aiming) {
	case amNever:
		break;

	case amOnKick:
		if (drawCursor) {
			if (kickType == ktLeft) {
				aimcursor.SetPos(character.X() + LFOOT_OFFSET_X - AIMCURSOR_OFFSET,
						 character.Y() + LFOOT_OFFSET_Y - AIMCURSOR_OFFSET);
				aimcursor.Draw();
			}
			else if (kickType == ktRight) {
				aimcursor.SetPos(character.X() + RFOOT_OFFSET_X - AIMCURSOR_OFFSET,
						 character.Y() + RFOOT_OFFSET_Y - AIMCURSOR_OFFSET);
				aimcursor.Draw();
			}
		}
		break;

	case amAlways:
		aimcursor.SetPos(character.X() + LFOOT_OFFSET_X - AIMCURSOR_OFFSET,
				 character.Y() + LFOOT_OFFSET_Y - AIMCURSOR_OFFSET);
		aimcursor.Draw();
		aimcursor.SetPos(character.X() + RFOOT_OFFSET_X - AIMCURSOR_OFFSET,
				 character.Y() + RFOOT_OFFSET_Y - AIMCURSOR_OFFSET);
		aimcursor.Draw();
		break;
	}
}

void Player::Undraw() {
	character.Undraw();
	// Aimcursor doesn't need to be undrawn seperately
	// because it is on the character's area
}

void Player::Walk(WalkDir wd) {
	character.Walk(wd);
	switch (wd) {
	case wdNone:
		movement = 0;
		break;

	case wdLeft:
		movement = -MOVE_SPEED;
		break;

	case wdRight:
		movement = MOVE_SPEED;
		break;
	}
}

WalkDir Player::Walking() const {
	if (!movement)
		return wdNone;

	if (movement < 0)
		return wdLeft;
	else
		return wdRight;
}

void Player::StartPowerPoll(KickType kt) {
	powerbar.StartPoll(kt);
	kickType = kt;
	drawCursor = true;
}

int Player::Kick(KickType kt) {
	int power = powerbar.EndPoll(kt);
	if (power) {
		updates = 0; // forces update
		character.Kick(kt);
		drawCursor = false;
	}

	return power;
}
