#ifndef STATS_H
#define STATS_H

class Stats {
public:
	void Draw();
	void Undraw();
	void Reset();
	void Update();
};

#endif // STATS_H
