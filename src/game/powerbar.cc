#include "color.h"
#include "defines.h"
#include "error.h"
#include "powerbar.h"

PowerBar::PowerBar(SDL_Surface *s, GameData *data, int number):
 screen(s) {
	switch (number) {
	case 1:
		bar.x = SCR_RESO_X - PB_W - PB_BORDER_DIST;
		break;

	case 2:
		bar.x = PB_BORDER_DIST;
		break;

	default:
		throw Error("Invalid player number");
	}
	bar.y = SCR_RESO_Y - PB_H - PB_BORDER_DIST;
	bar.w = PB_W;
	bar.h = PB_H;

	SDL_FillRect(data->bg, &bar, PB_BG_COLOR);

	bar.x += 1;
	bar.y += 1;
	bar.w -= 2;
	bar.h -= 2;

	SDL_FillRect(data->bg, &bar, PB_BORDER_COLOR);

	bar.x += 1;
	bar.y += 1;
	bar.w -= 2;
	bar.h -= 2;

	SDL_FillRect(data->bg, &bar, PB_BG_COLOR);
}

PowerBar::~PowerBar() {
}

void PowerBar::Reset() {
	poll = 0;
	Undraw();
}

void PowerBar::Update() {
	if (!poll)
		return;

	poll += 2;
	if (poll > bar.w)
		poll = bar.w;
}

void PowerBar::Draw() {
	SDL_Rect dst(bar);
	dst.w = poll;

	Uint32 color = 0;
	switch (kickType) {
	case ktHead:
		color = poll * 4 / 5 + 128;
		break;
	case ktLeft:
		color = (poll / 2 + 127) << 16;
		break;
	case ktRight:
		color = (poll / 2 + 127) << 8;
		break;
	}

	SDL_FillRect(screen, &dst, color);
}

void PowerBar::StartPoll(KickType kt) {
	kickType = kt;
	poll = 1;
	Undraw();
}

int PowerBar::EndPoll(KickType kt) {
	Undraw();

	if (kt == kickType) {
		int tmp = poll;
		poll = 0;
		return tmp;
	}
	return 0;
}
