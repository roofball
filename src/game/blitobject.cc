#include "blitobject.h"

BlitObject::BlitObject(SDL_Surface *gs, GameData *gd):
 data(gd),
 screen(gs) {
	rect.x = rect.y = rect.w = rect.h = 0;
	rect2.x = rect2.y = rect2.w = rect2.h = 0;
	loop = once = NULL;
}

void BlitObject::Draw() {
	Animation *anim = CurrentAnim();
	rect = rect2;
	if (anim)
		SDL_BlitSurface(anim->GetFrame(), NULL, screen, &rect);
}

void BlitObject::Undraw() {
	SDL_BlitSurface(data->bg, &rect, screen, &rect);
}

void BlitObject::PollFrame() {
	Animation *anim = CurrentAnim();
	if (anim)
		anim->PollFrame();
}

void BlitObject::Move(Sint16 x, Sint16 y) {
	rect2.x += x;
	rect2.y += y;
}

void BlitObject::MoveX(Sint16 x) {
	rect2.x += x;
}

void BlitObject::MoveY(Sint16 y) {
	rect2.y += y;
}

void BlitObject::SetPos(Sint16 x, Sint16 y) {
	rect2.x = x;
	rect2.y = y;
}

void BlitObject::SetLoopAnim(Animation *a) {
	loop = a;
}

void BlitObject::PlayAnimOnce(Animation *a) {
	once = a;
	if (once)
		once->FirstFrame();
}

Uint16 BlitObject::Width() {
	Animation *anim = CurrentAnim();
	if (anim)
		return anim->Width();
	else
		return 0;
}

Uint16 BlitObject::Height() {
	Animation *anim = CurrentAnim();
	if (anim)
		return anim->Height();
	else
		return 0;
}

Sint16 BlitObject::X() const {
	return rect2.x;
}

Sint16 BlitObject::Y() const {
	return rect2.y;
}

Animation *BlitObject::CurrentAnim() {
	if (once) {
		if (once->HasLooped()) {
			once = NULL;
			return CurrentAnim();
		}
		return once;
	}
	return loop;
}
