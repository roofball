#ifndef POWERBAR_H
#define POWERBAR_H

#include "color.h"
#include "gamedata.h"
#include "kicktype.h"
#include "defines.h"
#include "SDL.h"

class PowerBar {
public:
	PowerBar(SDL_Surface *, GameData *, int);
	~PowerBar();

	void Reset();
	void Update();

	void Draw();
	inline void Undraw();

	void StartPoll(KickType);
	int EndPoll(KickType);

private:
	SDL_Surface *screen;
	SDL_Rect bar;
	KickType kickType;
	int poll;
};

void PowerBar::Undraw() {
	SDL_FillRect(screen, &bar, Color(PB_BG_COLOR));
}

#endif // POWERBAR_H
