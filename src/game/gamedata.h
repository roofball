#ifndef GAMEDATA_H
#define GAMEDATA_H

#include "SDL.h"
#include "SDL_tbmf.h"
#include "chain.h"

using namespace sdl_tbmf;

class GameData {
public:
	typedef Chain<SDL_Surface *> FrameList;

	GameData(const char *);
	~GameData();

	SDL_Surface *bg;
	FrameList aimcursors[10];
	FrameList
		ball,
		neighbor,
		player_stand,
		player_walk,
		player_kh,
		player_kl,
		player_kr;

	Font *font;
};

#endif // GAMEDATA_H
