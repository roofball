#include "charactercolors.h"
#include "character.h"
#include "error.h"
#include "defines.h"
#include <iostream>

SDL_Surface *ConvertToColors(SDL_Surface *, const CharacterColors &);

Character::Character(SDL_Surface *s, GameData *gd):
 BlitObject(s, gd) {
	struct {
		GameData::FrameList *src;
		Animation *ani;
	} pair[5] = {
		{ &gd->player_stand, &stand },
		{ &gd->player_walk, &walk },
		{ &gd->player_kh, &kh },
		{ &gd->player_kl, &kl },
		{ &gd->player_kr, &kr }
	};

	CharacterColors c = { 0xffffff, 0xffffff, 0xffffff, 0xffffff };
	colors = c;

	for (unsigned i = 0; i < 5; ++i) {
		pair[i].src->NewPoll();
		while(pair[i].src->PollNext()) {
			pair[i].ani->AddFrame(ConvertToColors(pair[i].src->Current(), colors));
		}
	}

	SetLoopAnim(&stand);
}


void Character::Walk(WalkDir wd) {
	switch (wd) {
	case wdNone:
		SetLoopAnim(&stand);
		return;

	case wdLeft:
		walk.SetRate(-1);
		walk.FirstFrame();
		walk.SkipFrameBack();
		break;

	case wdRight:
		walk.SetRate(1);
		walk.LastFrame();
		break;
	}
	SetLoopAnim(&walk);
}

void Character::Kick(KickType kt) {
	switch (kt) {
	case ktHead:
		PlayAnimOnce(&kh);
		break;
	case ktLeft:
		PlayAnimOnce(&kl);
		break;
	case ktRight:
		PlayAnimOnce(&kr);
		break;
	}
}

const CharacterColors &Character::GetColors() const {
	return colors;
}

void Character::SetColors(const CharacterColors &c) {
	struct {
		GameData::FrameList *src;
		Animation *ani;
	} pair[5] = {
		{ &data->player_stand, &stand },
		{ &data->player_walk, &walk },
		{ &data->player_kh, &kh },
		{ &data->player_kl, &kl },
		{ &data->player_kr, &kr }
	};

	colors = c;

	for (unsigned i = 0; i < 5; ++i) {
		if (pair[i].src->Size() != pair[i].ani->FrameCount())
			throw Error("Data source animation size is different than used animation size");

		pair[i].ani->FirstFrame();
		pair[i].src->NewPoll();
		while(pair[i].src->PollNext()) {
			pair[i].ani->ReplaceFrame(ConvertToColors(pair[i].src->Current(), c));
			pair[i].ani->SkipFrame();
		}
	}
}

void ColorizePixel(SDL_Surface *, Uint32 *, SDL_Color, Uint8);

SDL_Surface *ConvertToColors(SDL_Surface *input, const CharacterColors &colors) {
	Uint8 r, g, b;

	Uint32 pixel_count, *pixel;
	SDL_Surface *output;

	// DisplayFormat makes a COPY
	output = SDL_DisplayFormat(input);
	if (!output) throw Error("Can't make a copy of player graphic");

	pixel_count = output->w * output->h + 1;
	pixel = (Uint32*)output->pixels;

	if (SDL_LockSurface(output))
		throw Error("Unable to lock player surface");

	while (--pixel_count) {
		SDL_GetRGB(*pixel, output->format, &r, &g, &b);

		if ((r == 0) && (g == 0xff) && (b == 0)) {
			++pixel;
			continue;
		}
		if (r && (g == 0) && (b == 0)) ColorizePixel(output, pixel, colors.hair, r);
		else if ((r == g) && (b == 0)) ColorizePixel(output, pixel, colors.shirt, r);
		else if ((r == b) && (g == 0)) ColorizePixel(output, pixel, colors.pants, r);
		else if ((g == b) && (r == 0)) ColorizePixel(output, pixel, colors.shoes, g);

		++pixel;
	}

	SDL_UnlockSurface(output);

	return output;
}

void ColorizePixel(SDL_Surface *input, Uint32 *pixel, SDL_Color color, Uint8 gam) {
	*pixel = SDL_MapRGB(input->format,
		color.r * gam / 255,
		color.g * gam / 255,
		color.b * gam / 255
	);
}
