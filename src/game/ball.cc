#include "game.h"
#include "ball.h"
#include "defines.h"
#include <cmath>

Ball::Ball(SDL_Surface *s, GameData *gd):
 BlitObject(s, gd) {
	data->ball.NewPoll();
	while(data->ball.PollNext())
		anim.AddFrame(data->ball.operator SDL_Surface *&());

	SetLoopAnim(&anim);

	Reset();
}

Ball::~Ball() {
}

void Ball::Reset() {
	SetPos((SCR_RESO_X - anim.Width()) / 2, 0);
	x = X();
	y = Y();
	vx = 0.0;
	vy = 0.0;
	updates = 0;
	anim.FirstFrame();
	SetSpin(0);
}

void Ball::LaunchNew() {
	if (x + BALL_DIAMETER - 2 < 0) {
		vx = 1.0;
		vy = 0.0;
		x = 0.0;
		y = 0.0;
		return;
	}
	if (x > SCR_RESO_X - 2) {
		vx = -1.0;
		vy = 0.0;
		x = SCR_RESO_X - BALL_DIAMETER;
		y = 0.0;
		return;
	}
	Reset();
}

int Ball::Update() {
	++updates;
	PollFrame();

	vy += 0.028;
	vy *= 0.9973;
	vx *= 0.9973;
	y += vy;
	x += vx;

	long double h = y + BALL_DIAMETER - GROUND_LEVEL;
	if (h > 0) {
		y = -h + GROUND_LEVEL - BALL_DIAMETER;
		vy *= -0.74;
		vx *= 0.94;
		SetSpin(0.4 * GetSpin() + vx * 0.39);
	}

	if ((h > -1.1) && (abs(vy) < 0.6)) {
		if (abs(vx) < 0.002)
			return 1;
		else
			vy = 0;
	}

	if (x + BALL_DIAMETER + 5 < 0)
		return 1;
	else if (x > SCR_RESO_X + 5)
		return 2;

	SetPos(int(x + 0.5), int(y + 0.5));
	return 0;
}

int Ball::Kick(KickType kt, int p, Sint16 kx, Sint16 ky) {
	long double
		dx = x + BALL_OFFSET - kx,
		dy = y + BALL_OFFSET - ky,
		d = sqrt((double)dx*dx + dy*dy);

	if (kt == ktHead) {
		// wether d < BallRadius + HeadRadius
		if ((d < BALL_OFFSET + HEAD_OFFSET_Y) && (dy < abs(dx) - 3)) {
			vx =+ sqrt((double)p) * dx * 0.02 - spin * 0.27;
			//vx = -0.07 * vx + sqrt((double)p) * (BALL_OFFSET - dx) * 0.02 * spin);
			vy = -0.12 * abs(vy) - sqrt((double)p) * 0.26 + dx * dx * spin * 0.01;
			SetSpin(spin * 0.25 + dx * abs(dx) * 0.0005 * sqrt((double)p));
			return 1;
		}
	}
	else {
		if ( (d < BALL_OFFSET) && ((dy < 2) || (dy < abs(dx))) ) {
			vx =+ sqrt((double)p + 3) * dx * 0.03 - spin * 0.04;
			//vx = -0.7 * vx + sqrt((double)p + 3) * dx * 0.024;
			vy = -0.32 * abs(vy) - 0.3 * sqrt((double)p - 1);
			SetSpin(spin * 0.21 + dx * abs(dx) * 0.002 * sqrt((double)p));
			return 1;
		}
	}

	return 0;
}

void Ball::SetSpin(long double s) {
	spin = s;
	anim.SetSpeed(s);
}

long double Ball::GetSpin() const {
	return spin;
}
