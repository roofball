#include "defines.h"
#include "game.h"

Game::Game(SDL_Surface *s, const char *p, Options *opt):
 data(new GameData(p)),
 options(opt),
 screen(s),
 pl1(s, data, &opt->pl1, 1),
 pl2(s, data, &opt->pl2, 2),
 ball(s, data) {
}

Game::~Game() {
}

void Game::Run(ResumeMode &mode) {
	// Refresh entire screen to game graphics
	SDL_BlitSurface(data->bg, NULL, screen, NULL);

	SDL_Event events;
	Uint32 lTicks;
	int ticks;

	SDL_EnableKeyRepeat(0, 0);

	if (options->pl1.update) {
		pl1.UpdateGraph(options->pl1.GetColors());
		options->pl1.update = false;
	}
	if (options->pl2.update) {
		pl2.UpdateGraph(options->pl2.GetColors());
		options->pl2.update = false;
	}

	while (mode == rmGame) {
		lTicks = SDL_GetTicks();

		stats.Undraw();
		ball.Undraw();
		pl1.Undraw();
		pl2.Undraw();

		HandleEvents(events, mode);

		ball.Update(); // Supersample ball for better quality
		if (int stat = ball.Update()) {
			//if (stat == 2)
				//neighbor.Launch();

			ball.LaunchNew();
		}
		pl1.Update();
		pl2.Update();
		stats.Update();

		ball.Draw();
		pl1.Draw();
		pl2.Draw();
		stats.Draw();

		SDL_Flip(screen);

		ticks = 34 - (SDL_GetTicks() - lTicks);
		if (ticks > 0) SDL_Delay(ticks);
	}
}

void Game::DrawBG() {
	SDL_BlitSurface(data->bg, NULL, screen, NULL);
}

void Game::Reset() {
	ball.Reset();
	pl1.Reset();
	pl2.Reset();
	stats.Reset();
}

void Game::HandleEvents(SDL_Event &event, ResumeMode &mode) {
	while (SDL_PollEvent(&event))
	switch (event.type) {
	case SDL_KEYDOWN:
		HandlePlayerButtons(event.key.keysym.sym, mode, true);
		break;

	case SDL_KEYUP:
		HandlePlayerButtons(event.key.keysym.sym, mode, false);
		break;

	case SDL_QUIT:
		mode = rmQuit;
		break;

	default:
		break;	
	}
}

void Game::HandlePlayerButtons(const SDLKey &key, ResumeMode &mode, bool keyPress) {
	Player *pl;

	for (int plNum = 1; plNum <= 2; ++plNum) {
		switch (plNum) {
		case 1:
			pl = &this->pl1;
			break;

		case 2:
			pl = &this->pl2;
			break;

		default:
			throw Error("What is this player number offered? (Game::HandlePlayerKeys)");
		}

		const PlayerKeys *keys = &pl->opt->keys;

		if (keyPress) {
			if (key == SDLK_ESCAPE) { mode = rmMenu; return; }
			if (key == keys->left) { pl->Walk(wdLeft); return; }
			if (key == keys->right) { pl->Walk(wdRight); return; }
			if (key == keys->kickUp) { pl->StartPowerPoll(ktHead); return; }
			if (key == keys->kickLeft) { pl->StartPowerPoll(ktLeft); return; }
			if (key == keys->kickRight) { pl->StartPowerPoll(ktRight); return; }
		}
		else {
			if (key == keys->left) { if (pl->Walking() == wdLeft) pl->Walk(wdNone); return; }
			if (key == keys->right) { if (pl->Walking() == wdRight) pl->Walk(wdNone); return; }

			int power;

			if (key == keys->kickUp) {
				power = pl->Kick(ktHead);
				if (power) ball.Kick(ktHead, power, pl->X() + HEAD_OFFSET_X,
								    pl->Y() + HEAD_OFFSET_Y);
				return;
			}

			if (key == keys->kickLeft) {
				power = pl->Kick(ktLeft);
				if (power) ball.Kick(ktLeft, power, pl->X() + LFOOT_OFFSET_X,
								    pl->Y() + LFOOT_OFFSET_Y);
				return;
			}

			if (key == keys->kickRight) {
				power = pl->Kick(ktRight);
				if (power) ball.Kick(ktRight, power, pl->X() + RFOOT_OFFSET_X,
								     pl->Y() + RFOOT_OFFSET_Y);
				return;
			}
		}

	}
}
