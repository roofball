#ifndef KICKTYPE_H
#define KICKTYPE_H

enum WalkDir { wdNone, wdLeft, wdRight };
enum KickType { ktHead, ktLeft, ktRight };

#endif // KICKTYPE_H
