#ifndef PLAYER_H
#define PLAYER_H

#include "aimcursor.h"
#include "character.h"
#include "playeroptions.h"
#include "powerbar.h"
#include "kicktype.h"

class Player {
public:
	Player(SDL_Surface *, GameData *, PlayerOptions const *, int);
	~Player() {}

	void Reset();

	void Update();
	void UpdateGraph(const CharacterColors &);

	void Draw();
	void Undraw();

	void Walk(WalkDir);
	WalkDir Walking() const;

	void StartPowerPoll(KickType);
	int Kick(KickType);


	Sint16 X() const { return character.X(); }
	Sint16 Y() const { return character.Y(); }

	PlayerOptions const *opt;
private:
	Character character;
	AimCursor aimcursor;
	PowerBar powerbar;
	int number, movement;
	const AimMode &aiming;
	bool drawCursor;
	KickType kickType;
	int updates;
};

#endif // PLAYER_H
