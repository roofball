#ifndef OPTIONS_H
#define OPTIONS_H

#include "optionsfile.h"
#include "playeroptions.h"
#include "SDL.h"
#include <string>

class Options {
public:
	Options(const char *);
	~Options();

	void Load();
	void Save();

	PlayerOptions pl1;
	PlayerOptions pl2;
	bool fullScreen;

private:
	OptionsFile *oFile;
};

#endif // OPTIONS_H
