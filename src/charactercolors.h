#ifndef CHARACTERCOLORS_H
#define CHARACTERCOLORS_H

#include "color.h"

struct CharacterColors {
	Color hair;
	Color shirt;
	Color pants;
	Color shoes;
};

#endif // CHARACTERCOLORS_H
