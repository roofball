#ifndef GETGRAPHIC_H
#define GETGRAPHIC_H

#include "SDL.h"

void GetGraphic(SDL_Surface **, const char *);

#endif // GETGRAPHIC_H
