#ifndef PLAYEROPTIONS_H
#define PLAYEROPTIONS_H

#include "enums.h"
#include "charactercolors.h"

struct PlayerKeys {
	SDLKey left;
	SDLKey right;
	SDLKey kickUp;
	SDLKey kickLeft;
	SDLKey kickRight;
};

struct PlayerOptions {
	PlayerOptions();
	~PlayerOptions();

	void SetStyle(const char *);
	const char *GetName() const;
	const CharacterColors &GetColors() const;

	AimMode aim;
	PlayerKeys keys;

	bool update;

private:
	CharacterColors colors;
	char name[32];
};

#endif // PLAYEROPTIONS_H
