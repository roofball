#include "color.h"

Color::Color(Uint32 c) {
	color = c;
}

Color::Color(SDL_Color c) {
	color = (c.unused << 24) | (c.r << 16) | (c.g << 8) | c.b;
}
