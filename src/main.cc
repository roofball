#include "enums.h"
#include "game.h"
#include "menu.h"
#include "error.h"
#include "chainerror.h"
#include "SDL.h"
#include "defines.h"
#include "options.h"
#include "optionsfile_errors.h"
#include <iostream>

void DisplayLoading(SDL_Surface *);

int main(int argc, char *argv[]) {
	if (argc > 1) {
		if (strcmp(argv[1], "--version") == 0) {
			cout << VERSION << endl;
			return 0;
		}
		if (strcmp(argv[1], "--help") == 0) {
			cout << "Usage: roofball [OPTION]\n";
			cout << "\t--version\tPrints version information and exits\n";
			cout << "\t--help\t\tPrints this help text and exits\n";
			return 0;
		}
		else {
			cout << argv[1] << ": unrecognized option '" << argv[1] << "'\n";
			cout << "Try '" << argv[0] << " --help' for more information\n";
			return 1;
		}
	}

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		cout << "SDL init error: " << SDL_GetError() << endl;
		return 1;
	}

	SDL_Surface *screen = SDL_SetVideoMode(SCR_RESO_X, SCR_RESO_Y, SCR_BPP, 0);
	if (screen == NULL) {
		cerr << "Unable to set " << SCR_RESO_X << 'x' << SCR_RESO_Y << 'x'
		     << SCR_BPP << " video mode: " << SDL_GetError() << endl;
		return 1;
	}


	SDL_WM_SetCaption("Roofball v" VERSION, NULL);

	try {
		DisplayLoading(screen);

		ResumeMode mode = rmMenu;
		Options options("options");
		Game game(screen, DATADIR, &options);
		Menu menu(screen, DATADIR, &options);

		menu.Init(game, mode);
		game.DrawBG();

		if (options.fullScreen)
			SDL_WM_ToggleFullScreen(screen);
		SDL_ShowCursor(SDL_DISABLE);

		while (true) {
			switch (mode) {
			case rmMenu:
				menu.Run(mode);
				break;

			case rmGame:
				game.Run(mode);
				break;

			case rmQuit:
				SDL_Quit();
				return 0;
			}
		}
	}
	catch (Error &e) {
		cerr << "Error encountered!\n";
		e.PrintError();
	}
	catch (ChainError &e) {
		cerr << "Chain error encountered!\n";
		switch (e) {
		case ceNoMem:
			cerr << "Not enough memory\n";
			break;

		case ceNoSuchNode:
			cerr << "No such node\n";
			break;
		}
	}
	catch (OFError &e) {
		e.Print();
	}
	catch (const char *e) {
		cerr << e << endl;
	}
	catch (...) {
		cerr << "Unknown error!\n";
	}

	SDL_Quit();
	return 1;
}

void DisplayLoading(SDL_Surface *s) {
	SDL_FillRect(s, NULL, 0);
	SDL_Surface *l = SDL_LoadBMP(DATADIR IMGDIR "loading.bmp");
	if (!l) throw Error("Can't find data");
	SDL_Rect lrect = { (SCR_RESO_X - l->w) / 2, (SCR_RESO_Y - l->h) / 2, l->w, l->h };
	SDL_BlitSurface(l, NULL, s, &lrect);
	SDL_FreeSurface(l);
	SDL_UpdateRects(s, 1, &lrect);
}
