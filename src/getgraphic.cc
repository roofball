#include "getgraphic.h"
#include "defines.h"
#include "error.h"

void GetGraphic(SDL_Surface **s, const char *p) {
	if (!s) throw Error("No address to load graphic surface pointer to");
	SDL_Surface *tmp = SDL_LoadBMP(p);
	if (!tmp) throw Error(etFileOpen, p);

	*s = SDL_DisplayFormat(tmp);
	if (!*s) throw Error("GetGraphic(): Could not convert to display format");
	SDL_SetColorKey(*s, SDL_SRCCOLORKEY, ALPHA_COLOR);
	SDL_FreeSurface(tmp);
}
