#include "optionsfile.h"
#include "optionsfile_errors.h"
#include <cstring>
#include <cstdio>
#include <cstdlib>

inline bool IsWhiteSpace(int c);
inline bool IsBlank(int c);

OptionsFile::StrList::StrList() {
	head = cur = NULL;
}

OptionsFile::StrList::~StrList() {
	Clear();
}

void OptionsFile::StrList::Clear() {
	Node *t, *s;
	t = head;

	while (t) {
		s = t;
		t = t->next;
		delete s->keyword;
		delete s->value;
		delete s;
	}
	head = cur = NULL;
}

bool OptionsFile::StrList::IsClear() const {
	return (head != NULL);
}

void OptionsFile::StrList::Set(const char *key, const char *val) {
	Node *n = new Node;
	if (!n) throw;
	n->value = new char[strlen(val) + 1];
	strcpy(n->value, val);
	n->keyword = new char[strlen(key) + 1];
	strcpy(n->keyword, key);
	n->next = NULL;

	if (!head) {
		head = cur = n;
		return;
	}

	if (strcasecmp(key, head->keyword) < 0) {
		n->next = head;
		head = n;
		return;
	}

	Node *t, *s;
	t = s = head;
	while (t && strcasecmp(key, t->keyword) > 0) {
		s = t;
		t = t->next;
	}
	if (!t) {
		s->next = n;
		return;
	}
	if (!strcasecmp(key, t->keyword)) {
		delete t->value;
		t->value = n->value;
		delete n->keyword;
		delete n;
		return;
	}
	s->next = n;
	n->next = t;
}

char *OptionsFile::StrList::Get(const char *key) const {
	if (!head) {
		return NULL;
	}
	if (strcasecmp(key, head->keyword) < 0) {
		return NULL;
	}

	Node *t = head;
	while (t && strcasecmp(key, t->keyword) > 0) {
		t = t->next;
	}
	if (!t) {
		return NULL;
	}
	if (!strcasecmp(key, t->keyword)) {
		return t->value;
	}
	return NULL;
}

void OptionsFile::StrList::Unset(const char *key) {
	if (!head) {
		return;
	}
	if (strcasecmp(key, head->keyword) < 0) {
		return;
	}

	Node *t, *s;
	t = s = head;
	while (t && strcasecmp(key, t->keyword) > 0) {
		s = t;
		t = t->next;
	}
	if (!t) {
		return;
	}
	if (!strcasecmp(key, t->keyword)) {
		s->next = t->next;
		delete t->keyword;
		delete t->value;
		delete t;
	}
}


void OptionsFile::StrList::StartPoll() {
	cur = head;
}

int OptionsFile::StrList::PollNext(char **key, char **val) {
	if (!cur) return 0;
	*key = cur->keyword;
	*val = cur->value;
	cur = cur->next;
	return 1;
}

OptionsFile::OptionsFile(const char *file, bool load, bool saveOnDel):
 autoSave(saveOnDel),
 isSaved(false) {
	fileName = new char[strlen(file) + 1];
	if (!fileName)
		throw OFError("Out of memory");
	strcpy(fileName, file);
	if (load) Load();
}

OptionsFile::~OptionsFile() {
	if (autoSave) Save();
}

void OptionsFile::Clear() {
	opts.Clear();
	isSaved = false;
}

bool OptionsFile::IsClear() const {
	return opts.IsClear();
}

void OptionsFile::Set(const char *sect, const char *key, const char *val) {
	char *kw = CreateKeyword(sect, key);
	opts.Set(kw, val);
	delete kw;
	isSaved = false;
}

char *OptionsFile::Get(const char *sect, const char *key) const {
	char *kw = CreateKeyword(sect, key);
	char *r = opts.Get(kw);
	delete kw;
	return r;
}

void OptionsFile::SetUInt(const char *sect, const char *key, UIntT val) {
	char str[64];
	if (snprintf(str, 64, "%llu", val) < 0)
		throw OFError("Integer value exceeds 64 characters");
	Set(sect, key, str);
}

void OptionsFile::SetUIntHex(const char *sect, const char *key, UIntT val) {
	char str[64];
	if (snprintf(str, 64, "0x%llx", val) < 0)
		throw OFError("Integer value exceeds 64 characters");
	Set(sect, key, str);
}

void OptionsFile::SetSInt(const char *sect, const char *key, SIntT val) {
	char str[64];
	if (snprintf(str, 64, "%lld", val) < 0)
		throw OFError("Integer value exceeds 64 characters");
	Set(sect, key, str);
}

void OptionsFile::SetFloat(const char *sect, const char *key, FloatT val) {
	char str[64];
	if (snprintf(str, 64, "%Lf", val) < 0)
		throw OFError("Floating point value exceeds 64 characters");
	Set(sect, key, str);
}

void OptionsFile::SetString(const char *sect, const char *key, StringT Ptr) {
	Set(sect, key, Ptr);
}

UIntT OptionsFile::GetUInt(const char *sect, const char *key) const {
	char *str = Get(sect, key);
	if (!str) return 0;
	UIntT r = strtoul(str, NULL, 0);
	return r;
}

SIntT OptionsFile::GetSInt(const char *sect, const char *key) const {
	char *str = Get(sect, key);
	if (!str) return 0;
	return atoll(str);
}

FloatT OptionsFile::GetFloat(const char *sect, const char *key) const {
	char *str = Get(sect, key);
	if (!str) return 0.0f;
	throw OFError("Floating point not yet implemented");
	return 0.0;
}

StringT OptionsFile::GetString(const char *sect, const char *key) const {
	StringT r = Get(sect, key);
	return r;
}

void OptionsFile::Unset(const char *sect, const char *key) {
	char *kw;
	kw = CreateKeyword(sect, key);
	opts.Unset(kw);
	delete kw;
	isSaved = false;
}

char *OptionsFile::CreateKeyword(const char *sect, const char *key) const {
	size_t len = strlen(sect) + strlen(key) + 2;
	char *s = new char[len];
	if (!s) throw;
	bzero(s, len);
	strcat(s, sect);
	strcat(s, "\001");
	strcat(s, key);
	return s;
}

void OptionsFile::SeparateKeyword(const char *keyword, char **sect, char **key) const {
	size_t len, len1, len2;

	len = strlen(keyword);
	for (len1 = 0; len1 < len; ++len1)
		if (keyword[len1] == 1) break;
	len2 = len - len1 - 1;

	*sect = new char[len1 + 1];
	*key = new char[len2 + 1];
	if (!*sect || !*key) throw;

	bzero(*sect, len1 + 1);
	bzero(*key, len2 + 1);
	strncpy(*sect, keyword, len1);
	strncpy(*key, keyword + len1 + 1, len2);
}

void OptionsFile::Save() {
	if (isSaved) return;
	open(fileName, ios::out | ios::binary | ios::trunc);
	if (!is_open()) throw OFError("Cannot open file for writing.");
	clear();
	char *k, *sect, *key, *v, *lastSect = NULL;
	opts.StartPoll();
	if (opts.PollNext(&k, &v)) {
		SeparateKeyword(k, &sect, &key);
		lastSect = sect;
		write("[", 1);
		write(sect, strlen(sect));
		write("]\n", 2);

		write(key, strlen(key));
		write(" = ", 3);
		write(v, strlen(v));
		write("\n", 1);
	}
	while (opts.PollNext(&k, &v)) {
		SeparateKeyword(k, &sect, &key);
		if (strcmp(sect, lastSect)) {
			write("\n[", 2);
			write(sect, strlen(sect));
			write("]\n", 2);
		}
		lastSect = sect;
		write(key, strlen(key));
		write(" = ", 3);
		write(v, strlen(v));
		write("\n", 1);
	}
	isSaved = true;
	close();
}

void OptionsFile::Load() {
	open(fileName, ios::in | ios::binary);

	char sect[OF_STRMAXLEN];
	char key[OF_STRMAXLEN];
	char val[OF_STRMAXLEN];
	char c;

	Clear();

	int wrongFormat = 0;
	while (!fail()) {
		if (!NextSignificant()) break;
		get(c);
		if (c != '[') {
			wrongFormat = 1;
			break;
		}
		sect[0] = 0;
		wrongFormat = 2;
		while(1) {
			get(c);
			if (fail()) break;
			if (c == ']') {
				wrongFormat = 0;
				break;
			}
			AddChar(sect, c, OF_STRMAXLEN);
			//else take char to sektion string
		}
		if (wrongFormat) break;
		ParseString(sect, OF_STRMAXLEN);
		while (!fail()) {
			if (!NextSignificant()) break;
			get(c);
			if (c == '[') {
				putback(c);
				break;
			}
			key[0] = 0;
			wrongFormat = 3;
			do {
				if (c == '=') {
					wrongFormat = 0;
					break;
				}
				AddChar(key, c, OF_STRMAXLEN);
				get(c);
			}
			while (!fail());
			if (wrongFormat) break;
			val[0] = 0;
			do {
				if (fail()) break;
				get(c);
			}
			while (IsBlank(c));
			if (fail()) break;
			//if (c == '[') {
			//	wrongFormat = 4;
			//	break;
			//}
			do {
				if (c == '\n') {
					wrongFormat = 0;
					break;
				}
				AddChar(val, c, OF_STRMAXLEN);
				get(c);
			}
			while (!fail());
			if (wrongFormat) break;
			ParseString(key, OF_STRMAXLEN);
			ParseString(val, OF_STRMAXLEN);
			Set(sect, key, val);
		}
		if (wrongFormat) break;
	}
	isSaved = !wrongFormat;

	if (wrongFormat) throw OFError(ofeWrongFormat, wrongFormat);
	close();
}

int OptionsFile::NextSignificant() {
	char c;
	int comment = 0;

	while (1) {
		get(c);
		if (fail()) break;
		if (comment) {
			if (c == '\n') comment = 0;
			continue;
		}
		if (IsWhiteSpace(c)) continue;
		if (c == '#') {
			comment = 1;
			continue;
		}
		putback(c);
		return 1;
	}

	return 0;
}

void OptionsFile::AddChar(char *str, char ch, size_t len) {
	size_t i = 0;
	if (!len) throw OFError(ofeTooLongString);
	for (i = 0; i < len; ++i) {
		if (str[i] == 0) break;
	}
	if (!(++i < len)) throw OFError(ofeTooLongString);
	str[i - 1] = ch;
	str[i] = 0;
}

void OptionsFile::ParseString(char *str, size_t len) {
	size_t i = 0;
	if (!len) return;
	for (i = 0; str[i] != 0; ++i) {
		if (!(i < len)) return;
	}
	if (!i) return;

	// remove trailing spaces
	while (--i >= 0) {
		if (!IsWhiteSpace(str[i])) break;
	}
	str[++i] = 0;

	// replace \x with x
	// <not done yet>
}

bool IsWhiteSpace(int c) {
	return (c == ' ' || c == '\t' || c == '\n');
}

bool IsBlank(int c) {
	return (c == ' ' || c == '\t');
}
