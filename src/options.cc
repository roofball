#include "options.h"

#include "optionsfile.h"
#include "error.h"
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>

#define PATHNAME_LEN 1024

Options::Options(const char *fileName) {
	char file[PATHNAME_LEN];

	const char *oDir = ".roofball/";
	const char *home = getenv("HOME");
	if (!home) {
		home = ".";
	}

	if (strlen(home) + strlen(oDir) + strlen(fileName) + 2 > PATHNAME_LEN)
		throw Error("Pathname for options file too long");

	strcpy(file, home);
	strcat(file, "/");
	strcat(file, oDir);
	mkdir(file, 0755);
	strcat(file, fileName);

	// Set default options
	fullScreen = true;

	pl1.SetStyle("Lake");
	pl1.aim = amOnKick;
	pl1.keys.left = SDLK_LEFT;
	pl1.keys.right = SDLK_RIGHT;
	pl1.keys.kickUp = SDLK_UP;
	pl1.keys.kickLeft = SDLK_MODE;
	pl1.keys.kickRight = SDLK_RCTRL;

	pl2.SetStyle("Heke");
	pl2.aim = amOnKick;
	pl2.keys.left = SDLK_d;
	pl2.keys.right = SDLK_g;
	pl2.keys.kickUp = SDLK_r;
	pl2.keys.kickLeft = SDLK_LCTRL;
	pl2.keys.kickRight = SDLK_LALT;

	oFile = new OptionsFile(file);
	if (!oFile) throw Error(etNoMem);

	Load();
}

Options::~Options() {
	Save();
	delete oFile;
}

void Options::Load() {
	if (oFile->Has("Misc", "FullScreen"))
		fullScreen = oFile->GetUInt("Misc", "FullScreen");

	if (oFile->Has("Player 1", "Style"))
		pl1.SetStyle(oFile->GetString("Player 1", "Style"));
	if (oFile->Has("Player 1", "Key Left"))
		pl1.keys.left = (SDLKey)oFile->GetUInt("Player 1", "Key Left");
	if (oFile->Has("Player 1", "Key Right"))
		pl1.keys.right = (SDLKey)oFile->GetUInt("Player 1", "Key Right");
	if (oFile->Has("Player 1", "Key KickUp"))
		pl1.keys.kickUp = (SDLKey)oFile->GetUInt("Player 1", "Key KickUp");
	if (oFile->Has("Player 1", "Key KickLeft"))
		pl1.keys.kickLeft = (SDLKey)oFile->GetUInt("Player 1", "Key KickLeft");
	if (oFile->Has("Player 1", "Key KickRight"))
		pl1.keys.kickRight = (SDLKey)oFile->GetUInt("Player 1", "Key KickRight");
	if (oFile->Has("Player 1", "Aiming"))
		pl1.aim = (AimMode)oFile->GetUInt("Player 1", "Aiming");

	if (oFile->Has("Player 2", "Style"))
		pl2.SetStyle(oFile->GetString("Player 2", "Style"));
	if (oFile->Has("Player 2", "Key Left"))
		pl2.keys.left = (SDLKey)oFile->GetUInt("Player 2", "Key Left");
	if (oFile->Has("Player 2", "Key Right"))
		pl2.keys.right = (SDLKey)oFile->GetUInt("Player 2", "Key Right");
	if (oFile->Has("Player 2", "Key KickUp"))
		pl2.keys.kickUp = (SDLKey)oFile->GetUInt("Player 2", "Key KickUp");
	if (oFile->Has("Player 2", "Key KickLeft"))
		pl2.keys.kickLeft = (SDLKey)oFile->GetUInt("Player 2", "Key KickLeft");
	if (oFile->Has("Player 2", "Key KickRight"))
		pl2.keys.kickRight = (SDLKey)oFile->GetUInt("Player 2", "Key KickRight");
	if (oFile->Has("Player 2", "Aiming"))
		pl2.aim = (AimMode)oFile->GetUInt("Player 2", "Aiming");
}

void Options::Save() {
	oFile->SetUInt("Misc", "FullScreen", fullScreen);

	oFile->SetString("Player 1", "Style", pl1.GetName());
	oFile->SetUInt("Player 1", "Key Left", pl1.keys.left);
	oFile->SetUInt("Player 1", "Key Right", pl1.keys.right);
	oFile->SetUInt("Player 1", "Key KickUp", pl1.keys.kickUp);
	oFile->SetUInt("Player 1", "Key KickLeft", pl1.keys.kickLeft);
	oFile->SetUInt("Player 1", "Key KickRight", pl1.keys.kickRight);
	oFile->SetUInt("Player 1", "Aiming", pl1.aim);

	oFile->SetString("Player 2", "Style", pl2.GetName());
	oFile->SetUInt("Player 2", "Key Left", pl2.keys.left);
	oFile->SetUInt("Player 2", "Key Right", pl2.keys.right);
	oFile->SetUInt("Player 2", "Key KickUp", pl2.keys.kickUp);
	oFile->SetUInt("Player 2", "Key KickLeft", pl2.keys.kickLeft);
	oFile->SetUInt("Player 2", "Key KickRight", pl2.keys.kickRight);
	oFile->SetUInt("Player 2", "Aiming", pl2.aim);

	oFile->Save();
}
